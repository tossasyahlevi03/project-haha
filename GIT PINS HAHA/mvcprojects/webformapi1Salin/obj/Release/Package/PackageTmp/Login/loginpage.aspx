﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginpage.aspx.cs" Inherits="webformapi1.Login.loginpage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
        <meta charset="utf-8" />
     <meta http-equiv="Cache-Control" content="no-cache"/>
          <meta http-equiv="Pragma" content="no-cache"/>
          <meta http-equiv="Expires" content="0"/>
       <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
          <meta name="HandheldFriendly" content="true" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="stylelogin.css" type="text/css" />
       <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />
    <script src="../scripts/jquery-1.8.2.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
        <div class="row">
            <div class="column leftcol">

                <img src="../ASSET/payment-cards-png-6.png"class="poto" />
<%--                <img src="../ASSET/Make-your-payment-Icon.png"  class="poto" />--%>
            </div>

                        <div class="column">

      
    <div class="box-login">
        <table>
          
            <tr>
                <td><asp:TextBox runat="server" ID="txtusername" CssClass="form-control" placeholder="Username"></asp:TextBox></td>
            </tr>
               <tr>
                <td><asp:TextBox runat="server" ID="TextBox1" CssClass="form-control" placeholder="Password" TextMode="Password" ></asp:TextBox></td>
            </tr>
            <tr>
               
               
                <td>
                    <asp:Button runat="server" Text="Login" ID="btn_login" CssClass="btn" OnClick="btn_login_Click" />
                       <asp:LinkButton runat="server" ID="lb_forget" Text="Lupa Password ?" ForeColor="Red"></asp:LinkButton>
                </td>
                                      

            </tr>
            <tr>
                <td>
                    <label id="lb" runat="server" class="lberror"></label>
                </td>
            </tr>
          
        </table>
     <%--   <asp:GridView runat="server" ID="dg"></asp:GridView>--%>
      </div>

                        </div>
            </div>
    </div>
    </form>
</body>
</html>
