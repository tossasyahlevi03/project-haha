﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="webformapi1.Dashboard.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="styledashboard.css" type="text/css" />
      <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:TextBox runat="server" ID="lb" Text="asaa"></asp:TextBox>

      <div id="wrapper">
    <div class="breadcrumb">
      <a href="Dashboard.aspx"> Dashboard</a> 
    </div>

         
          <div class ="box-tile">
              
              <div class="tile1">
<%-- icon user--%>
                <a href="../User/User.aspx"> <img src="../ASSET/user-menu-512.png" class="hov" height="70" width="70" alt="Management User" title="Management User" /></a>
                    
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="lbcount1" Text="0"></asp:Label>
                     <p>Jumlah User</p> 
                  </div>
              </div>
               <div class="tile1">
                  
                  <%-- icon product--%>
<a href="../Product/Product.aspx"><img src="../ASSET/kissclipart-agile-methodology-icon-product-icon-release-icon-aae9433ba995367c.png" class="hov" height="30" width="30" alt="Product" title="Product" />    </a>                    
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="Label1" Text="0"></asp:Label>
                     <p>Jumlah Product</p> 
                      <label id="lberror1" runat="server"></label>
                
              
          </div>
              </div>

                       <div class="tile1">
                  
                           <%--icon invoice--%>

                          <a href="../Invoice/Invoice.aspx">  <img src="../ASSET/invoice-icon-54.png" width="30" class="hov" height="30" alt="Invoice" title="Invoice"  /></a>
                      
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="Label2" Text="0"></asp:Label>
                     <p>Jumlah Invoice</p> 
                                      <label id="errorinvoice" runat="server"></label>

              
          </div>
              </div>

                       <div class="tile1">
                  <%--icon license--%>
                            <img src="../ASSET/1649292.png" width="30" class="hov" height="30" alt="License" title="License"  />
                      
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="Label3" Text="0"></asp:Label>
                     <p>Jumlah License</p> 
                                      <label id="errorlicense" runat="server"></label>

              
          </div>
              </div>

                <div class="tile1">
                 <%-- icon customer--%>
                    <a href="../CustomerDetail/CustomerDetail.aspx"> <img src="../ASSET/unnamed.png" width="30" class="hov" height="30" alt="Log Customer Detail" title="Log Customer Detail"  />  </a> 
                      
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="Label4" Text="0"></asp:Label>
                     <p>Jumlah Customer</p> 
                                      <label id="errorcustomer" runat="server"></label>

              
          </div>
              </div>
                
              </div>
         

<%--          <div class="box-dashboard">
              <ul>
                  <li>
                    <a href="../User/User.aspx"><span> <img src="../ASSET/user-menu-512.png" class="hov" width="100" height="100" alt="Management User" title="Management User" /></span></a>  
                  </li>
                  <li>
                    <a href="../CustomerDetail/CustomerDetail.aspx"><span><img src="../ASSET/unnamed.png" width="100" class="hov" height="100" alt="Log Customer Detail" title="Log Customer Detail"  /> </span></a>  
                  </li>
                  <li>
                    <a href="#"><span><img src="../ASSET/invoice-icon-54.png" width="100" class="hov" height="100" alt="Invoice" title="Invoice"  /></span></a>  
                  </li>
                   <li>
                       <a href="#"> <img src="../ASSET/kissclipart-agile-methodology-icon-product-icon-release-icon-aae9433ba995367c.png" class="hov" height="100" alt="Product" title="Product" /></a>  
                  </li>
                    <li>
                    <a href="#"><span> <img src="../ASSET/1649292.png" width="100" class="hov" height="100" alt="License" title="License"  /></span></a>  
                  </li>
                 
              </ul>
          </div>--%>
        

    </div>
</asp:Content>
