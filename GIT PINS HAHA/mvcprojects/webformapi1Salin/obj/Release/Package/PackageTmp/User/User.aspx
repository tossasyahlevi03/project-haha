﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="webformapi1.User.User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="styleuser.css" type="text/css" />
   <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />

    <script src="../ASSET/bootstrap.js" type="text/javascript"></script>
    <script src="../ASSET/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.8.2.js"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="wrapper">
    <div class="breadcrumb">
      <a href="User.aspx">Management User</a> / <a href="TambahUser.aspx"> Tambah User</a>
    </div>



        <div class="box-data">

            <div class="box-control">

          <asp:DropDownList runat="server" EnableViewState="false" ID="dp_active" CssClass="drops" AutoPostBack="true" OnSelectedIndexChanged="dp_active_SelectedIndexChanged">
                    <asp:ListItem>--Select Status--</asp:ListItem>
                     <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>INACTIVE</asp:ListItem>
                </asp:DropDownList>
                    

             <div class="container">
                      <asp:TextBox runat="server" ID="txtcari" placeholder="Search By Nik" CssClass="ss"></asp:TextBox>
                        <div class="input-group-append">

       <asp:Button runat="server" ID="Button1" Text="Cari" CssClass="btn" OnClick="btncari_Click" data-toggle="modal" data-target="#exampleModal" />

</div>
                 

            </div>
               
               </div>
       
        <div class="dg1">
            <label runat="server" id="lberror"></label>
            <asp:GridView runat="server" AllowPaging="true" PageSize="10" EmptyDataText="Tidak ADA dATA" ID="dgrapor" CssClass="mygridview" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
        </div>
   
    </div>

    


    </div>
</asp:Content>
