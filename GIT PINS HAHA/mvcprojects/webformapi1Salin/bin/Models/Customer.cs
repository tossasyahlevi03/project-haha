﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformapi1.Models
{
    public class Customer
    {public string nik { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string pic_name { get; set; }
        public string pic_phone_number { get; set; }
        public string pic_email { get; set; }
        public string admin_owner { get; set; }
    }
}