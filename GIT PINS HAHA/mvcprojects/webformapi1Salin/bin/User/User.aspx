﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="webformapi1.User.User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="styleuser.css" type="text/css" />
   <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />

    <script src="../ASSET/bootstrap.js" type="text/javascript"></script>
    <script src="../ASSET/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.8.2.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script type="text/javascript">

      

        $(function () {
            $("[id*=dgrapor").find("[id*=links]").click(function () {
                //Reference the GridView Row.
                var row = $(this).closest("tr");
                $("#myModal2").modal();
               
                //Determine the Row Index.
                var message = "Row Index: " + (row[0].rowIndex - 1);

                //Read values from Cells.
             
                var nik = row.find("td").eq(0).html();
                var firstname = row.find("td").eq(1).html();
                var lastname = row.find("td").eq(2).html();
                var email = row.find("td").eq(3).html();
                var password = row.find("td").eq(4).html();
                var status = row.find("td").eq(5).html();
                var audittrail = row.find("td").eq(6).html();
                var role= row.find("td").eq(7).html();

                  $("#lb2").val(status);

                $("#<%=TextBox1.ClientID%>").val(nik);
               $("#<%=TextBox2.ClientID%>").val(firstname);
                $("#<%=TextBox3.ClientID%>").val(lastname);
                $("#<%=TextBox4.ClientID%>").val(email);
                $("#<%=TextBox5.ClientID%>").val(password);
                $("#<%=TextBox6.ClientID%>").val(status);
                  $("#<%=TextBox8.ClientID%>").val(role);

               <%-- if (status == "ACTIVE")
                {
                    $("#<%=Button4.ClientID%>").val("Set INACTIVE");
                  $("#<%=TextBox6.ClientID%>").val("ACTIVE");

                }
                else
                {
                    $("#<%=Button4.ClientID%>").val("Set ACTIVE");
               $("#<%=TextBox6.ClientID%>").val("INACTIVE");

                }--%>



                //Reference the TextBox and read value.
               // message += "\nCountry: " + row.find("td").eq(2).find("input").eq(0).val();

                //Display the data using JavaScript Alert Message Box.
               // alert(message);
                return false;
            });
        });
    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="wrapper">
    <div class="breadcrumb">
      <a href="User.aspx">Management User</a> / <a href="#" data-toggle="modal" data-target="#myModal">Tambah User</a>
    </div>


         <div id="myModal2" class="modal fade">
                 <div class="modal-dialog">

  <!-- Modal content -->
   <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Manipulasi Data User</h4>
        </div>
        <div class="modal-body">
           <div class="box">
             <div class="box-isi">
                Nomor Induk Kepegawaian
                 <asp:TextBox runat="server" CssClass="form-control"  ID="TextBox1"></asp:TextBox>
             </div>
             <div class="box-isi">
               First Name
                 <asp:TextBox runat="server" CssClass="form-control" ID="TextBox2"></asp:TextBox>
             </div>
             <div class="box-isi">
               Last Name
                 <asp:TextBox runat="server" CssClass="form-control" ID="TextBox3"></asp:TextBox>
             </div>
             <div class="box-isi">
               Email
                 <asp:TextBox runat="server" CssClass="form-control" ID="TextBox4"></asp:TextBox>
             </div>
             <div class="box-isi">
               Password
                 <asp:TextBox runat="server" CssClass="form-control" ID="TextBox5"></asp:TextBox>
             </div>
             <div class="box-isi">
               Status
                 <asp:TextBox runat="server" CssClass="form-control aax" Width="90" ID="TextBox6" Enabled="false" ></asp:TextBox>
             </div>
                  <div class="box-isi">
              Audit Trail
                 <asp:TextBox runat="server" CssClass="form-control" ID="TextBox7" Enabled="false" Text="-"></asp:TextBox>
             </div>
                <div class="box-isi">
              Role
                 <asp:TextBox runat="server" CssClass="form-control" ID="TextBox8" ></asp:TextBox>
             </div>
                  <div class="box-isi">
              
                 <asp:Button runat="server" ID="Button2" CssClass="btn btn-default" Text="Ubah" OnClick="Button2_Click" />
                                       <asp:Button runat="server" ID="Button3" CssClass="btn btn-default" Text="Delete" OnClick="Button3_Click" />
                   
                             

                      <label id="lb2" hidden="hidden"></label>
                      <label id="Label2" runat="server"></label>
             </div>
         </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

</div>
             </div>
             </div>




         <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah User</h4>
        </div>
        <div class="modal-body">
           <div class="box">
             <div class="box-isi">
                Nomor Induk Kepegawaian
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtnik"></asp:TextBox>
             </div>
             <div class="box-isi">
               First Name
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtfirstname"></asp:TextBox>
             </div>
             <div class="box-isi">
               Last Name
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtlastname"></asp:TextBox>
             </div>
             <div class="box-isi">
               Email
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtemail"></asp:TextBox>
             </div>
             <div class="box-isi">
               Password
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtpassword"></asp:TextBox>
             </div>
                <div class="box-isi">
               Nomor Telpon
                 <asp:TextBox runat="server" CssClass="form-control" ID="TextBox9"></asp:TextBox>
             </div>
             <div class="box-isi">
               Status
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtstatus" Enabled="false" Text="INACTIVE"></asp:TextBox>
             </div>
                  <div class="box-isi">
              Audit Trail
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtaudit" Enabled="false" Text="-"></asp:TextBox>
             </div>
               <div class="box-isi">
             Role
                <asp:DropDownList runat="server" ID="dprole">
                     <asp:ListItem>--Select Role--</asp:ListItem>
                     <asp:ListItem>Admin</asp:ListItem>
                    <asp:ListItem>Supervisor</asp:ListItem>
                     <asp:ListItem>Sales</asp:ListItem>
                </asp:DropDownList>
                    </div>
                  <div class="box-isi">
              
                 <asp:Button runat="server" ID="btnisi" CssClass="btn22" Text="Submit" OnClick="btnisi_Click"  />
                      <label id="Label1" runat="server"></label>
             </div>
         </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

        <div class="box-data">

            <div class="box-control">

          <asp:DropDownList runat="server" EnableViewState="false" ID="dp_active" CssClass="drops" AutoPostBack="true" OnSelectedIndexChanged="dp_active_SelectedIndexChanged">
                    <asp:ListItem>--Select Status--</asp:ListItem>
                     <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>NONACTIVE</asp:ListItem>
                </asp:DropDownList>
                    

             <div class="container">
                      <asp:TextBox runat="server" ID="txtcari" placeholder="Search By Nik" CssClass="ss"></asp:TextBox>
                        <div class="input-group-append">

       <asp:Button runat="server" ID="Button1" Text="Cari" CssClass="btn" OnClick="btncari_Click" data-toggle="modal" data-target="#exampleModal" />

</div>
                 

            </div>
               
               </div>
       
         <div class="table-responsive">

            <label runat="server" id="lberror"></label>
            <asp:GridView runat="server" AllowPaging="True" PageSize="4" EmptyDataText="Tidak ADA dATA" ID="dgrapor"   CssClass="mygridview"                  

                      AlternatingRowStyle-CssClass="alt"

                      PagerStyle-CssClass="pgr"  CellPadding="4"       OnPageIndexChanging="dgrapor_PageIndexChanging"  AutoGenerateColumns="False" ForeColor="#333333" GridLines="None">
<AlternatingRowStyle CssClass="alt" BackColor="White"></AlternatingRowStyle>
               <Columns>
                   <asp:BoundField DataField="nik" HeaderText="NIK" />
                   <asp:BoundField DataField="first_name" HeaderText="Nama Awal" />
                      <asp:BoundField DataField="last_name" HeaderText="Nama Akhir" />
                   <asp:BoundField DataField="email" HeaderText="Email" />
                   <asp:BoundField DataField="status" HeaderText="Status" />
   <asp:BoundField DataField="audit_trail" HeaderText="audit trail" />
                      <asp:BoundField DataField="role" HeaderText="role" />
           <asp:BoundField DataField="phone_number" HeaderText="Nomor Telpon" />


       <asp:TemplateField>
              <HeaderTemplate>
<%--                  <a href="#" data-toggle="modal" data-target="#myModal">Tambah User</a>--%>
              </HeaderTemplate>
              <ItemTemplate>
                   <asp:LinkButton runat="server" ID="links" Text="View" ></asp:LinkButton>
              </ItemTemplate>
        </asp:TemplateField>
  </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />


           <EmptyDataTemplate>No Record Available</EmptyDataTemplate>  

                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />

            </asp:GridView>
        </div>
   
    </div>

    


    </div>
</asp:Content>
