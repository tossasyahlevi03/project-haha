﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="webformapi1.Dashboard.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="styledashboard.css" type="text/css" />
      <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:TextBox runat="server" ID="lb" Text="asaa"></asp:TextBox>

      <div id="wrapper">
    <div class="breadcrumb cs">
      <a href="Dashboard.aspx"> Dashboard</a> 
    </div>


          <div class="dashboard-tile">


                    <div class="tile1 a">
<%-- icon user--%>
                <a href="../User/User.aspx"> <img src="../ASSET/user-menu-512.png" class="hov" height="70" width="70" alt="Management User" title="Management User" /></a>
                    
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="lbcount1" ForeColor="Blue" Font-Bold="true" Font-Size="12" Text="0"></asp:Label>
                    &nbsp;&nbsp;&nbsp; <p>User</p> 
                  </div>

              </div>

                           <div class="tile1">
                  
                  <%-- icon product--%>
<a href="../Product/Product.aspx"><img src="../ASSET/kissclipart-agile-methodology-icon-product-icon-release-icon-aae9433ba995367c.png" class="hov" height="30" width="30" alt="Product" title="Product" />    </a>                    
                  <div class="tiletile1">
                      <asp:Label runat="server" ForeColor="Blue" Font-Bold="true" Font-Size="12" ID="Label9" ></asp:Label>
                     <p>JProduct</p> 
                     &nbsp;&nbsp;&nbsp; <label id="Label14" runat="server"></label>
                
              
          </div>

              </div>

             <%-- icon invoice--%>

               <div class="tile1">
                  
                           <%--icon invoice--%>

                          <a href="../Invoice/Invoice.aspx">  <img src="../ASSET/invoice-icon-54.png" width="30" class="hov" height="30" alt="Invoice" title="Invoice"  /></a>
                      
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="Label2" ForeColor="Blue" Font-Bold="true" Font-Size="12" ></asp:Label>
                   &nbsp;&nbsp;&nbsp;  <p>Invoice</p> 

              
          </div>
              </div>

                <div class="tile1">
                  <%--icon license--%>
                         <a href="../License/License.aspx"><img src="../ASSET/1649292.png" width="30" class="hov" height="30" alt="License" title="License"  /></a>   
                      
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="Label3" ForeColor="Blue" Font-Bold="true" Font-Size="12"></asp:Label>
                  &nbsp;&nbsp;&nbsp;   <p>License</p> 

              
          </div>
              </div>


               <div class="tile1">
                  <%--icon license--%>
                    <a href="../CustomerDetail/CustomerDetail.aspx"><span><img src="../ASSET/unnamed.png" width="30" class="hov" height="30" alt="Log Customer Detail" title="Log Customer Detail"  /> </span></a>  
                      
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="Label1" ForeColor="Blue" Font-Bold="true" Font-Size="12" ></asp:Label>
                   &nbsp;&nbsp;&nbsp;  <p>Customer</p> 

              
          </div>
              </div>

                   <div class="tile1">
                  <%--icon license--%>
                    <a href="../CustomerDetail/CustomerDetail.aspx"><span><img src="../ASSET/unnamed.png" width="30" class="hov" height="30" alt="Log Customer Detail" title="Log Customer Detail"  /> </span></a>  
                      
                  <div class="tiletile1">
                      <asp:Label runat="server" ID="Label4" ForeColor="Blue" Font-Bold="true" Font-Size="12" ></asp:Label>
                   &nbsp;&nbsp;&nbsp;  <p>Complaint</p> 

              
          </div>
              </div>


          </div>


        <div class="box-complain">
            <div class="form-group">
                ID
                <asp:TextBox runat="server" ID="txtid" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                Type
                <asp:DropDownList runat="server" ID="dpp">
                  
                                  <asp:ListItem>COMPLAINT</asp:ListItem>
                   <asp:ListItem>NEWS</asp:ListItem>

                    
                      </asp:DropDownList>
            </div>
            <div class="from-group">
                From
             <asp:TextBox runat="server" ID="txtfrom" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                To
             <asp:TextBox runat="server" ID="txtto" CssClass="form-control"></asp:TextBox>

            </div>

            <div class="form-group">

                Date
                <asp:TextBox runat="server" ID="txtdate" CssClass="form-control"></asp:TextBox>

            </div>

            <div class="form-group">
                Subject
                <asp:TextBox runat="server" ID="txtsubject" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                Content
                <textarea class="form-control" id="txtconten" runat="server"></textarea>
            </div>

            <div class="form-group">
                Contact
                <asp:TextBox runat="server" ID="txtcontact" CssClass="form-control"></asp:TextBox>
            </div>

            <div class="form-group">
                Attachment
                <asp:FileUpload runat="server" ID="upload" CssClass="glyphicon-upload" />
                <asp:Button runat="server" ID="btns" Text="Upload" CssClass="btn btn-default" OnClick="btns_Click" />
                <asp:Label runat="server" ID="lbpath"></asp:Label>
            </div>

            <div class="form-group">
                 <asp:Button runat="server" ID="btnss" CssClass="btn btn-default" OnClick="btnss_Click" Text="Send" />
            </div>

        </div>

      



       
          

<%--          <div class="box-dashboard">
              <ul>
                  <li>
                    <a href="../User/User.aspx"><span> <img src="../ASSET/user-menu-512.png" class="hov" width="100" height="100" alt="Management User" title="Management User" /></span></a>  
                  </li>
                  <li>
                    <a href="../CustomerDetail/CustomerDetail.aspx"><span><img src="../ASSET/unnamed.png" width="100" class="hov" height="100" alt="Log Customer Detail" title="Log Customer Detail"  /> </span></a>  
                  </li>
                  <li>
                    <a href="#"><span><img src="../ASSET/invoice-icon-54.png" width="100" class="hov" height="100" alt="Invoice" title="Invoice"  /></span></a>  
                  </li>
                   <li>
                       <a href="#"> <img src="../ASSET/kissclipart-agile-methodology-icon-product-icon-release-icon-aae9433ba995367c.png" class="hov" height="100" alt="Product" title="Product" /></a>  
                  </li>
                    <li>
                    <a href="#"><span> <img src="../ASSET/1649292.png" width="100" class="hov" height="100" alt="License" title="License"  /></span></a>  
                  </li>
                 
              </ul>
          </div>--%>
        

    </div>
</asp:Content>
