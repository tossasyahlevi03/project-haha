﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using webformapi1.Models;
namespace webformapi1.License
{
    public partial class License : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {if(!IsPostBack)
            {
                get();
            }

        }
        protected void dgrapor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrapor.PageIndex = e.NewPageIndex;
            this.get();
        }
        public void get()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/licenses/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("getLicenses").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    //lberror.InnerText = s;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Licenses>>(s);
                    dgrapor.DataBind();
                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;

                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }
        public void insert()
        {

        }
        public void update()
        {

        }
        public void delete()
        {

        }
    }
}