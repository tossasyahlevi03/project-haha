﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using webformapi1.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Net.Mail;
using System.Security.Cryptography;

namespace webformapi1.Login
{
    public partial class loginpage : System.Web.UI.Page
    {
        Cdr cd = new Cdr();
        protected void Page_Load(object sender, EventArgs e)
        {

        }




     


        public void sendemail(string password)
        {
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential("tossasyahlevi03@gmail.com", "syntaxerror3");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("tossasyahlevi03@gmail.com");
            msg.To.Add(new MailAddress(txtemail.Text.Trim()));
            msg.Subject = "Recovery Password";
            msg.IsBodyHtml = true;
            msg.Body = string.Format("<html><head></head><body> Link Update Password : <a href=http://10.0.30.104:3212/ubahpassword/ubahpassword.aspx?nik=" + password+ "> Update Password</a></body>");
            try
            {
                client.Send(msg);
                lb.InnerText= "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
                lb.InnerText = "Error occured while sending your message." + ex.Message;
            }
        }

        public void getpasswrod()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cdr cd = new Cdr { email = txtemail.Text.Trim() };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                    var response = client.PostAsJsonAsync("getNikByEmail", cd).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";
                        var s = response.Content.ReadAsStringAsync().Result;
                        // var personList = JsonConvert.DeserializeObject<Cdr>(s);
                        if (s == null || s == "" || s == string.Empty)
                        {
                            lb.InnerText = "Anda Tidak Terdaftar";
                        }
                        else
                        {
                            string nik = s.Trim();
                            sendemail(nik);

                           
                        }
                    }
                    else
                    {
                        lb.InnerText = response.ReasonPhrase +" "+"API Tidak Terhubung";
                    }
                    
                   
                }
            }
            catch (Exception ex)
            {
                lb.InnerText = ex.Message + " " + "API Tidak Terhubung" + " " + "Periksa jaringan internet anda";
                //Response.Write("<script>alert('" + ex.Message + "' );</script>");

            }
        }

        public void Set_inactive()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cdr cd = new Cdr { nik = txtusername.Text};
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                    var response = client.PostAsJsonAsync("statusToInactive", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";

                      
                      


                    }
                    else
                    {
                        lb.InnerText = response.ReasonPhrase;

                    }
                }
            }
            catch (Exception ex)
            {
                lb.InnerText = ex.Message;
               


            }
        }

        public static string md5(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));
            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        public void GetLogin()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cdr cd = new Cdr { nik = txtusername.Text.Trim(), password =txtpassword.Text.Trim() };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                    var response = client.PostAsJsonAsync("login", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";
                        var s = response.Content.ReadAsStringAsync().Result;
                        var personList = JsonConvert.DeserializeObject<Cdr>(s);

                        string hashpass = md5(txtpassword.Text);

                        if (txtusername.Text == personList.nik && hashpass== personList.password && personList.status == "NONACTIVE" && personList.role == "Supervisor")
                        {

                            Session["email"] = personList.email;
                            Session["nik"] = personList.nik;
                            Session["first_name"] = personList.first_name;
                            Session["last_name"] = personList.last_name;
                            Session["password"] = personList.password;
                            Session["status"] = "ACTIVE";
                            Session["audit_trail"] = personList.audit_trail;
                            Session["role"] = personList.role;
                            Session["phone"] = "-";


                            Response.Redirect("../Dashboard/Dashboard.aspx");
                        }
                        if (txtusername.Text == personList.nik && hashpass == personList.password && personList.status == "NONACTIVE" && personList.role == "Admin")
                        {

                            Session["email"] = personList.email;
                            Session["nik"] = personList.nik;
                            Session["first_name"] = personList.first_name;
                            Session["last_name"] = personList.last_name;
                            Session["password"] = personList.password;
                            Session["status"] = "ACTIVE";
                            Session["audit_trail"] = personList.audit_trail;
                            Session["role"] = personList.role;
                            Session["phone"] = "-";


                            Response.Redirect("../Dashboard/Dashboard.aspx");
                        }
                        else
                        {
                           // lb.InnerText = response.ReasonPhrase + " " + " Atau Anda Sudah Login Di Tempat Lain" + " " + "Atau Anda Bukan Admin dan Supervisor";

                            if (txtusername.Text == personList.nik && hashpass == personList.password && personList.status == "ACTIVE")
                            {
                                lb.InnerText = "Anda Sudah Login Di Tempat Lain ";
                                Set_inactive();

                            }
                            if (txtusername.Text == personList.nik && hashpass == personList.password && personList.status == "NONACTIVE")
                            {
                                // Response.Redirect("http://10.0.30.104:8997/PageTes/Page.aspx");
                                lb.InnerText = "Anda Bukan Supervisor Atau Admin";
                                Set_inactive();

                            }


                        }
                    }

                    else
                    {
                        lb.InnerText = "Parameter Error  1) Gangguan Koneksi API 2): Gangguan Koneksi Internet - Silahkan Cek Pengaturan Jaringan Anda  3): Anda Tidak Terdaftar";
                        Set_inactive();

                    }
                }
            }
            catch (Exception ex)
            {
                 lb.InnerText = ex.Message +" "+"Periksa Jaringan Internet Anda";

            }
        }




        protected void btn_login_Click(object sender, EventArgs e)
        {
          if(txtusername.Text==null || txtusername.Text==string.Empty|| txtusername.Text=="" || txtpassword.Text==null||txtpassword.Text==string.Empty|| txtpassword.Text=="")
            {
                Response.Write("<script>alert('Username and Password cant be blank' );</script>");            
            }
          else
            {
                 GetLogin();
            }
           
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            getpasswrod();
        }
    }
}