﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="TambahUser.aspx.cs" Inherits="webformapi1.User.TambahUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="styletambahuser.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="wrapper">
    <div class="breadcrumb">
    <a href="User.aspx">Management User</a>  /   <a href="TambahUser.aspx">Tambah User</a> 
    </div>
         <div class="box">
             <div class="box-isi">
                Nomor Induk Kepegawaian
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtnik"></asp:TextBox>
             </div>
             <div class="box-isi">
               First Name
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtfirstname"></asp:TextBox>
             </div>
             <div class="box-isi">
               Last Name
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtlastname"></asp:TextBox>
             </div>
             <div class="box-isi">
               Email
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtemail"></asp:TextBox>
             </div>
             <div class="box-isi">
               Password
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtpassword"></asp:TextBox>
             </div>
             <div class="box-isi">
               Status
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtstatus" Enabled="false" Text="INACTIVE"></asp:TextBox>
             </div>
                  <div class="box-isi">
              Audit Trail
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtaudit" Enabled="false" Text="-"></asp:TextBox>
             </div>
                  <div class="box-isi">
              
                 <asp:Button runat="server" ID="btnisi" CssClass="btn" Text="Submit" OnClick="btnisi_Click" />
                      <label id="lberror" runat="server"></label>
             </div>
         </div>

</div>
</asp:Content>
