﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;

namespace webformapi1.UpdateProfil
{
    public partial class UpdatePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtemail.Text = Session["email"].ToString().Trim();
                txtnik.Text = Session["nik"].ToString();
                txtfirstname.Text = Session["first_name"].ToString();
                txtlastname.Text = Session["last_name"].ToString();
                txtpassword.Text = Session["password"].ToString();

                txtphonenumber.Text = Session["phone"].ToString();
            }

        }

        public void update()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    updateadmin cd = new updateadmin {  nik=txtnik.Text, first_name = txtfirstname.Text, last_name = txtlastname.Text, email = txtemail.Text, password = txtpassword.Text, phone_number = txtphonenumber.Text };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                    var response = client.PostAsJsonAsync("updateBasicInfo", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";
                        var s = response.Content.ReadAsStringAsync().Result;

                      //  Response.Write(s);
                        lb.InnerText = s;
                        // Get_All_User();

                    }
                    else
                    {
                        // Response.Write("<script>alert('" + response.ReasonPhrase + "');</script>");

                        lb.InnerText = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception e)
            {
                lb.InnerText = e.Message;
            }

        }

        protected void btnsimpan_Click(object sender, EventArgs e)
        {
            update();
        }
    }
}