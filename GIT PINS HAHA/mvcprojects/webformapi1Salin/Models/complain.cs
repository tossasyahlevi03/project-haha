﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformapi1.Models
{
    public class complain
    {
        public string report_id { get; set; }
        public string report_type { get; set; }
        public string froms { get; set; }
        public string tos { get; set; }
        public DateTime dates { get; set; }
        public string subject { get; set; }
        public string contents { get; set; }
        public string contacts { get; set; }
        public string attachment { get; set; }
    }
}