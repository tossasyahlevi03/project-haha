﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformapi1.Models
{
    public class Products
    {
        public int product_id { get; set; }
        public string name { get; set; }
        public string period { get; set; }
        public float fee_per_period { get; set; }
        public float disc { get; set; }
        public float afterDisc { get; set; }
    }
}