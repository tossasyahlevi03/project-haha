﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformapi1.Models
{
    public class Licenses
    {
        public string customer_nik { get; set; }
        public int product_id { get; set; }
        public string product_name { get; set; }
        public string license_number { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string status { get; set; }
    }
}