﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
namespace webformapi1.Product
{
    public partial class Product : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                loads();
            }
        }


        public void loads()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/product/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("getProducts").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                   // lberror.InnerText = s;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Products>>(s);
                    dgrapor.DataBind();
                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;

                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }
    }
    }

   
