﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
namespace webformapi1.Masterpage
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        Cdr cd = new Cdr();
        public string nik { get; set; }
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["first_name"].ToString() == null || Session["last_name"].ToString() == null || Session["email"].ToString() == null ||
                    Session["status"].ToString() == null)
                {

                    Response.Redirect("../login/loginpage.aspx");
                }
                else
                {
                    string nama1 = Session["first_name"].ToString();
                    string nama2 = Session["last_name"].ToString();
                    nik = Session["nik"].ToString();
                    string email = Session["email"].ToString();
                    string status = Session["status"].ToString();
                    lbnama.Text = nama1 + " " + nama2;
                    lbjabatan.Text = Session["email"].ToString();
                    lbnik.Text = nik;
                    lbstatus.Text = status;
                    lbrole.Text = Session["role"].ToString();
                }

               

            }
        }

      public void Set_inactive()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cdr cd = new Cdr { nik = lbnik.Text };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                    var response = client.PostAsJsonAsync("statusToInactive", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";

                        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        HttpContext.Current.Response.Cache.SetNoServerCaching();
                        HttpContext.Current.Response.Cache.SetNoStore();
                        Session.Abandon();
                      

                        Response.Redirect("../Login/loginpage.aspx");


                    }
                    else
                    {
                        Response.Write("<script>alert('"+response.ReasonPhrase+"' );</script>");

                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "' );</script>");


            }
        }

        protected void btnlogout_Click(object sender, EventArgs e)
        {
            Set_inactive();
           
        }

        protected void linkb_Click(object sender, EventArgs e)
        {

            string url = "../CustomerDetail/CustomerDetail.aspx?NIK=" + Session["nik"].ToString(); 

            Response.Redirect(url);
        }
    }
}