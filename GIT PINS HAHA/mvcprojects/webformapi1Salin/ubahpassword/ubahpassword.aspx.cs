﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
namespace webformapi1.ubahpassword
{
    public partial class ubahpassword : System.Web.UI.Page
    {
        public string niks { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
           if(!IsPostBack)
            {

                string a = HttpContext.Current.Request.Url.PathAndQuery;

                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                // http://localhost:1302/TESTERS/Default6.aspx

               niks= Request.QueryString["nik"];
                lbs.InnerText = Request.QueryString["nik"];
               

            }
              
            
        }
        public void ubahpass()
        {
            using (var client = new HttpClient())
            {
                Cdr cd = new Cdr { nik= Request.QueryString["nik"], password = TextBox1.Text };
                client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                var response = client.PostAsJsonAsync("updatePassword", cd).Result;
                if (response.IsSuccessStatusCode)
                {
                    // lb.InnerText = "koneksi";
                    var s = response.Content.ReadAsStringAsync().Result;

                    //  Response.Write(s);
                    lb.InnerText = s;
                    // Get_All_User();

                }
                else
                {
                    // Response.Write("<script>alert('" + response.ReasonPhrase + "');</script>");
                    lb.InnerText = response.ReasonPhrase;
                    // lberror.InnerText = response.ReasonPhrase;
                }
            }

        }

        protected void btn_login_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "" || TextBox1.Text == null)
            {

                Response.Write("<script>alert('tidak boleh kosong' );</script>");


            }
            else
            {
                ubahpass();
            }
        }
    }
}