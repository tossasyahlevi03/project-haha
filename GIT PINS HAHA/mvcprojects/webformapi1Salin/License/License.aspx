﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="License.aspx.cs" Inherits="webformapi1.License.License" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="stylelicense.css" type="text/css" />
        <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div id="wrapper">
    <div class="breadcrumb">
      <a href="Product.aspx">Management Products </a>  / <a href="#" data-toggle="modal" data-target="#myModal">Tambah Products</a>
    </div>
            <div class="box-data">

            <div class="box-control">
                  <div class="container">
                      <asp:TextBox runat="server" ID="txtcari" placeholder="Search By Invoice Number" CssClass="ss"></asp:TextBox>
                        <div class="input-group-append">

       <asp:Button runat="server" ID="Button1" Text="Cari" CssClass="btn"  data-toggle="modal" data-target="#exampleModal" />

</div>
                 

            </div>
               
               </div>
       
        <div class="dg1">
                        <label runat="server" id="lberror"></label>
            <asp:GridView runat="server" ID="dgrapor" EmptyDataText="No Data" PageSize="4"  AllowPaging="true" PageIndex="3" OnPageIndexChanging="dgrapor_PageIndexChanging" CssClass="table table-striped"

                   

                      AlternatingRowStyle-CssClass="alt"

                      PagerStyle-CssClass="pgr" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
              
                 <Columns>
                   <asp:BoundField DataField="customer_nik" HeaderText="Customer NIK" />
                   <asp:BoundField DataField="product_id" HeaderText="Product ID" />
                      <asp:BoundField DataField="product_name" HeaderText="Nama Product" />
                   <asp:BoundField DataField="license_number" HeaderText="License Number" />
   <asp:BoundField DataField="start_date" HeaderText="Start Date" />
                   <asp:BoundField DataField="end_date" HeaderText="End Date" />
                        <asp:BoundField DataField="status" HeaderText="Status" />


       <asp:TemplateField>
              <HeaderTemplate>
<%--                  <a href="#" data-toggle="modal" data-target="#myModal">Tambah User</a>--%>
              </HeaderTemplate>
              <ItemTemplate>
                   <asp:LinkButton runat="server" ID="links" Text="View" ></asp:LinkButton>
              </ItemTemplate>
        </asp:TemplateField>
  </Columns>

                
                  <AlternatingRowStyle BackColor="White" />
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
                                    <EmptyDataTemplate>No Record Available</EmptyDataTemplate>  

            </asp:GridView>
     </div>
     </div>
         </div>
</asp:Content>
