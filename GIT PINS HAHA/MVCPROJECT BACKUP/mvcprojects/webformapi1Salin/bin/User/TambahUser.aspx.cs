﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using webformapi1.Models;
using Newtonsoft.Json;

namespace webformapi1.User
{
    public partial class TambahUser : System.Web.UI.Page
    {
        informasi ins = new informasi();
        protected void Page_Load(object sender, EventArgs e)
        { if(!IsPostBack)
            {

            }

        }

        public void Post_Insert()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cdr cd = new Cdr { nik = txtnik.Text, first_name = txtfirstname.Text, last_name = txtlastname.Text, email = txtemail.Text, password = txtpassword.Text, status = txtstatus.Text, audit_trail = txtaudit.Text };
                    client.BaseAddress = new Uri("http://localhost:58324/dummy/");
                    var response = client.PostAsJsonAsync("Add_User", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";

                        txtnik.Text = "";
                        txtlastname.Text = "";
                        txtemail.Text = "";
                        txtpassword.Text = "";
                        txtfirstname.Text = "";
                        lberror.InnerText = "Penyimpanan Berhasil";

                    }
                    else
                    {
                        lberror.InnerText = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                lberror.InnerText = ex.Message;

            }
        }


        protected void btnisi_Click(object sender, EventArgs e)
        {
            if (txtaudit.Text==null||txtemail.Text==null||txtfirstname.Text==null||txtlastname.Text==null||txtnik.Text==null||txtpassword.Text==null||txtstatus.Text==null||txtaudit.Text==null)
            {
                Response.Write("<script>alert(' Data Harus Diisi' );</script>");

            }
        else
            {
                Post_Insert();
            }

        }
    }
}