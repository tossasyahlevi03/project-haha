﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="UpdatePassword.aspx.cs" Inherits="webformapi1.UpdateProfil.UpdatePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="styleupdatepassword.css" />
      <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />

    <script src="../ASSET/bootstrap.js" type="text/javascript"></script>
    <script src="../ASSET/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.8.2.js"></script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="wrapper">
        <div class="breadcrumb">
            <a href="UpdatePassword.aspx"> Update Personal Account</a>
        </div>

        <div class="container">
            <div class="boxkiri2 batas">
                
                <asp:Image runat="server" ID="imgs" ImageUrl="~/ASSET/1649292.png" CssClass="imgss" />
                <div class="form-group">
                    <asp:Button runat="server" ID="btnsimpan" CssClass="btn btn-default" OnClick="btnsimpan_Click" Text="Update" />
                </div>
            </div>
            <div class="boxkiri">
                <div class="form-group">
                    NIK
                    <asp:TextBox runat="server" placeholder="NIK" CssClass="form-control" ID="txtnik"></asp:TextBox>
                </div>
                 <div class="form-group">
                     Nama Awal
                    <asp:TextBox runat="server" placeholder="First Name" CssClass="form-control" ID="txtfirstname"></asp:TextBox>
                </div>
                 <div class="form-group">
                     Nama Akhir
                    <asp:TextBox runat="server" placeholder="Last Name" CssClass="form-control" ID="txtlastname"></asp:TextBox>
                </div>
                 <div class="form-group">
                     Email
                    <asp:TextBox runat="server" placeholder="Email" CssClass="form-control" ID="txtemail"></asp:TextBox>
                </div>
                 <div class="form-group">
                     Password
                    <asp:TextBox runat="server" placeholder="Password" CssClass="form-control" ID="txtpassword"></asp:TextBox>
                </div>
               
                <div class="form-group">
                    Phone Number
                    <asp:TextBox runat="server" placeholder="Phone Number" CssClass="form-control" ID="txtphonenumber"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label runat="server" id="lb"></label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
