﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformapi1.Models
{
    public class updateadmin
    {
        public string nik { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone_number { get; set; }
        public string password { get; set; }
        public string email { get; set; }
    }
}