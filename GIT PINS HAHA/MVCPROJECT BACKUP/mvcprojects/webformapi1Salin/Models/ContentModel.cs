﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformapi1.Models
{
    public class ContentModel
    {
        public string contents { get; set; }
        public string contacts { get; set; }
        public string attachment { get; set; }
    }
}