﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
namespace webformapi1.Invoice
{
    public partial class Invoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                GetInvoices();
            }
        }

        protected void dgrapor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrapor.PageIndex = e.NewPageIndex;
           
        }


        public void insert()
        {

        }

        public void update()
        {

        }
        public void delete()
        {
            try
            {
                using (var client = new HttpClient())
                {
                   Invoices cd = new Invoices{ customer_nik = txtnik.Text};
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/customers/");
                    var response = client.PostAsJsonAsync("deleteCustomer", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                      //  txtnamacustomer.Text = txtcity.Text = txtaddress.Text = txtpicemail.Text = txtpicname.Text = txtpicphone.Text = txtprovinsi.Text = "";
                        Response.Write("<script>alert(' Penyimpanan Berhasil' );</script>");
                    }
                    else
                    {
                        lberror.InnerText = response.ReasonPhrase + "Periksa Koneksi Anda Tidak Dapat Terhubung Ke API ";
                    }
                }
            }
            catch (Exception ex)
            {
                lberror.InnerText = ex.Message;
            }
        }

        public void GetInvoices()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/invoices/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("getInvoices").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Invoices>>(s);
                    dgrapor.DataBind();
                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;

                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }


    }
}