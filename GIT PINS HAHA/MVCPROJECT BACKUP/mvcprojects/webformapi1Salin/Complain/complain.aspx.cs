﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
namespace webformapi1.Complain
{
    public partial class complain : System.Web.UI.Page
    {
        public string filepath { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                TextBox4.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
               
                getall();


                
            }
        }

       

        public void simpan()
        {
          
            try
            {
                using (var client = new HttpClient())
                {
                    webformapi1.Models.complain  cd = new webformapi1.Models.complain { report_id= TextBox1.Text, report_type= DropDownList1.SelectedValue, froms=TextBox2.Text, tos=TextBox3.Text, dates=DateTime.Parse(this.TextBox4.Text), subject= TextBox5.Text, contents=Textarea1.InnerText, contacts=TextBox6.Text };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                    var response = client.PostAsJsonAsync("insertAdmin", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";

                       
                        Response.Write("<script>alert(' Penyimpanan Berhasil' );</script>");


                    }
                    else
                    {
                        lberror.InnerText = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                lberror.InnerText = ex.Message;

            }
        }

        public void getall()
        {
            try
            {
                webformapi1.Models.complain cd = new webformapi1.Models.complain { tos = Session["email"].ToString() };
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/report/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.PostAsJsonAsync("getComplaintByReceiver",cd).Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    // lberror.InnerText = s;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<webformapi1.Models.complain>>(s);
                    dgrapor.DataBind();
                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;

                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }

        protected void btnloadpc_Click(object sender, EventArgs e)
        {
            string folderpath = Server.MapPath("~/IMAGES2/");
            if (!Directory.Exists(folderpath))
            {
                Directory.CreateDirectory(folderpath);
               
            }
            var fp = Path.Combine(folderpath, Path.GetFileName( FileUpload1.FileName));
          
                upload.SaveAs(fp);

            
            filepath = folderpath + Path.GetFileName(FileUpload1.FileName);
            lbpathfile.InnerText = filepath;
            Label2.InnerText = filepath;
        }

        protected void links_Click(object sender, EventArgs e)
        {
            //string url = "../Complain/DetaIlComplain.aspx?ID=" + Session["nik"].ToString();

         
            //Response.Redirect(url);

        }
    }
}