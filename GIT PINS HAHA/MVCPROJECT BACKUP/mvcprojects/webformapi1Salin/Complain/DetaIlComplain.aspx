﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="DetaIlComplain.aspx.cs" Inherits="webformapi1.Complain.DetaIlComplain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="styledetailcomplaint.css" />
     <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />

    <script src="../ASSET/bootstrap.js" type="text/javascript"></script>
    <script src="../ASSET/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.8.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div id="wrapper">
		<div class="breadcrumb">
			<a href="complain.aspx">Complaint</a> / <a href="DetaIlComplain.aspx">Detail Complaint</a>  
		</div>

          <div class="box">
              <div class="form-group ass">
                  <asp:TextBox runat="server" ID="txtform" class="form-control" placeholder="Pengirim"></asp:TextBox>
             <asp:TextBox runat="server" ID="txtdate" class="form-control" placeholder="Date Sent"></asp:TextBox>
           <asp:TextBox runat="server" ID="txttype" class="form-control" placeholder="Type Messages"></asp:TextBox>

                    </div>
                <div class="form-group">
                  <asp:TextBox runat="server" ID="txtpenerima" class="form-control" placeholder="Penerima"></asp:TextBox>
              </div>
                <div class="form-group">
                  <asp:TextBox runat="server" ID="txtsubject" class="form-control" placeholder="Subjek"></asp:TextBox>
              </div>
                <div class="form-group">
                  <textarea id="txtisi" runat="server" class="form-control"></textarea>
              </div>
              <div class="form-group">
                  <asp:Image runat="server" ID="ims" CssClass="gambars"  />
                  <asp:LinkButton runat="server" ID="imss" Text="View Image" OnClick="imss_Click"></asp:LinkButton>
              </div>

              <div class="form-group">
                  <asp:Button runat="server" ID="btnreply" Text="Reply" CssClass="btn btn-default" />
         
                    <asp:Button runat="server" ID="delete" Text="Delete" CssClass="btn btn-default" />

                  <label runat="server" id="lberror"></label>
              
              </div>

          </div>
          </div>
</asp:Content>
