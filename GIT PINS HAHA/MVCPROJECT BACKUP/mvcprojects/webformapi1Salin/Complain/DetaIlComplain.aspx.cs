﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
namespace webformapi1.Complain
{
    public partial class DetaIlComplain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtform.Text = Request.QueryString["ID"];
                txtpenerima.Text = Request.QueryString["from"];
                txtsubject.Text = Request.QueryString["subject"];
                txtdate.Text = Request.QueryString["date"];
                txttype.Text = Request.QueryString["type"];
                getisi();
            }

        }
        public string paths { get; set; }
        public void getisi()
        {
            try
            {
               webformapi1.Models.complain  cd = new webformapi1.Models.complain { report_id= Request.QueryString["ID"]};
          

                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.PostAsJsonAsync("report/getContentById", cd).Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    var personList = JsonConvert.DeserializeObject<webformapi1.Models.ContentModel>(s);
                    txtisi.InnerText = personList.contents;
                    loadgambar(personList.attachment);
                  lberror.InnerText = personList.attachment;

                }
                else
                {
                    lberror.InnerText = rs.ReasonPhrase;


                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }

            
        }

        public void loadgambar(string pathss)
        {
           // string folderpath = Server.MapPath("~/IMAGES/");
           // ims.ImageUrl = "~/IMAGES/" + Path.GetFileName(pathss);

            ims.ImageUrl = Path.GetFileName(pathss);
        }

        protected void imss_Click(object sender, EventArgs e)
        {
            string url = "../viewimage/view.aspx?path=" + lberror.InnerText;

            Response.Redirect(url);
        }
    }
}