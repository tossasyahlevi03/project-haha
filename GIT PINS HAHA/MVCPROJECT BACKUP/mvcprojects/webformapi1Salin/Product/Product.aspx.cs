﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
namespace webformapi1.Product
{
    public partial class Product : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                loads();
            }
        }


        public void simpanproduct()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Products cd = new Products { product_id=Convert.ToInt32(this.txtproductid.Text), name=txtproductname.Text,period=dd.SelectedValue, fee_per_period = float.Parse(this.txtharga.Text) };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/product/");
                    var response = client.PostAsJsonAsync("insertProduct", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";

                     
                        Response.Write("<script>alert(' Penyimpanan Berhasil' );</script>");


                    }
                    else
                    {
                        lberror.InnerText = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                lberror.InnerText = ex.Message;

            }
        }

        public void loads()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/product/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("getProducts").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                   // lberror.InnerText = s;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Products>>(s);
                    dgrapor.DataBind();
                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;

                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }

        protected void dgrapor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrapor.PageIndex = e.NewPageIndex;
            this.loads();
        }

        protected void btnisi_Click(object sender, EventArgs e)
        {
            simpanproduct();
        }

        protected void btnisi_Click1(object sender, EventArgs e)
        {
            simpanproduct();
        }
    }
    }

   
