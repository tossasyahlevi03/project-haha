﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
using System.Net.Http.Formatting;
using Newtonsoft.Json;

namespace webformapi1.User
{
    public partial class User : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Get_All_User();
              
            }
        }


        public void Get_All_User()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("admin/getAdmins").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Cdr>>(s);
                    dgrapor.DataBind();

                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;
                    Response.Write("<script>alert('" + rs.ReasonPhrase + "' );</script>");

                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;
                Response.Write("<script>alert('" + ex.Message + "' );</script>");

            }
        }


        public void Get_All_User_NIK(string nik)
        {
            try
            {
                Cdr cd = new Cdr { nik = nik };

                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.PostAsJsonAsync("admin/getAdmin", cd).Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Cdr>>(s);
                    dgrapor.DataBind();

                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;
                   

                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }

        public void Get_User_active_Post()
        {
            try
            {
                

                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("admin/getActiveAdmins").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Cdr>>(s);
                    dgrapor.DataBind();

                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;


                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }

       public void get_user_inactive_post()
        {
            try
            {


                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("admin/getNonActiveAdmins").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Cdr>>(s);
                    dgrapor.DataBind();

                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;


                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }




        protected void dp_active_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(dp_active.SelectedValue=="Active")
            {
                Get_User_active_Post();

            }
            if (dp_active.SelectedValue=="NONACTIVE")
            {
                get_user_inactive_post();

            }
        }

        protected void btncari_Click(object sender, EventArgs e)
        {
            if (txtcari.Text == "" || txtcari.Text == null || txtcari.Text == string.Empty)
            {
                Response.Write("<script>alert(' Kolom NIK Harus Diisi' );</script>");

            }
            else
            {
                Get_All_User_NIK(txtcari.Text.Trim());
            }
        }

   

        protected void dgrapor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrapor.PageIndex = e.NewPageIndex;
            this.Get_All_User();
        }


        public void Set_Inactive(string nik)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cdr cd = new Cdr { nik = nik };
                    client.BaseAddress = new Uri("http://localhost:58324/dummy/");
                    var response = client.PostAsJsonAsync("Set_Inactive", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";

                      
                        Response.Write("<script>alert(' User Dengan Nik'"+TextBox1.Text+"'Telah Di Non-aktifkan' );</script>");
                        Get_All_User();

                    }
                    else
                    {
                        Response.Write("<script>alert('"+ response.ReasonPhrase+"' );</script>");

                        lberror.InnerText = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('"+ex.Message+"' );</script>");

                lberror.InnerText = ex.Message;

            }
        }


        public void Set_Active(string nik)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cdr cd = new Cdr { nik = nik };
                    client.BaseAddress = new Uri("http://localhost:58324/dummy/");
                    var response = client.PostAsJsonAsync("Set_Active", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";


                        Response.Write("<script>alert(' User Dengan Nik'" + TextBox1.Text + "'Telah Di Non-aktifkan' );</script>");
                        Get_All_User();

                    }
                    else
                    {
                        Response.Write("<script>alert('" + response.ReasonPhrase + "' );</script>");

                        lberror.InnerText = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "' );</script>");

                lberror.InnerText = ex.Message;

            }
        }


        public void Post_Insert()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cdr cd = new Cdr { nik = txtnik.Text, first_name = txtfirstname.Text, last_name = txtlastname.Text, email = txtemail.Text, password = txtpassword.Text, status = txtstatus.Text, audit_trail = txtaudit.Text, role=dprole.SelectedValue, phone_number=TextBox9.Text};
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                    var response = client.PostAsJsonAsync("insertAdmin", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";

                        txtnik.Text = "";
                        txtlastname.Text = "";
                        txtemail.Text = "";
                        txtpassword.Text = "";
                        txtfirstname.Text = "";
                        Response.Write("<script>alert(' Penyimpanan Berhasil' );</script>");


                    }
                    else
                    {
                        lberror.InnerText = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
                lberror.InnerText = ex.Message;

            }
        }

        protected void btnisi_Click(object sender, EventArgs e)
        {
            if (txtaudit.Text == null || txtemail.Text == null || txtfirstname.Text == null || txtlastname.Text == null || txtnik.Text == null || txtpassword.Text == null || txtstatus.Text == null || txtaudit.Text == null)
            {
                Response.Write("<script>alert(' Data Harus Diisi' );</script>");

            }
            else
            {
                Post_Insert();
            }
        }

        public void setubah()
        {
            using (var client = new HttpClient())
            {
                Cdr cd = new Cdr { nik =TextBox1.Text,first_name=TextBox2.Text,last_name=TextBox3.Text,email=TextBox4.Text,password=TextBox5.Text,status=TextBox6.Text,audit_trail=TextBox7.Text,role=TextBox8.Text };
                client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                var response = client.PostAsJsonAsync("updateAdmin", cd).Result;
                if (response.IsSuccessStatusCode)
                {
                    // lb.InnerText = "koneksi";
                    var s = response.Content.ReadAsStringAsync().Result;

                    Response.Write(s);
                    Get_All_User();

                }
                else
                {
                    Response.Write("<script>alert('" + response.ReasonPhrase + "');</script>");

                   // lberror.InnerText = response.ReasonPhrase;
                }
            }

            }

        public void delete()
        {
            using (var client = new HttpClient())
            {
                Cdr cd = new Cdr { nik = TextBox1.Text };
                client.BaseAddress = new Uri("http://10.0.30.76:8080/admin/");
                var response = client.PostAsJsonAsync("deleteAdmin", cd).Result;
                if (response.IsSuccessStatusCode)
                {
                    // lb.InnerText = "koneksi";

                   
                    Response.Write("<script>alert(' Delete Success' );</script>");
                    Get_All_User();

                }
                else
                {
                    Response.Write("<script>alert('" + response.ReasonPhrase+"');</script>");

                    lberror.InnerText = response.ReasonPhrase;
                }
            }

            }

     
      

        protected void Button3_Click(object sender, EventArgs e)
        {
            delete();
            
        }

        protected void Button4_Click(object sender, EventArgs e)
        { string a= Request.Form["lb2"];


            if (a == "INACTIVE")
            {
                Set_Active(TextBox1.Text);
                Response.Write("<script>alert('"+a+"');</script>");
            }
            else
            {

                Set_Inactive(TextBox1.Text);
                Response.Write("<script>alert('" + a + "');</script>");
            }

        }



        protected void Button2_Click(object sender, EventArgs e)
        {
            setubah();
        }
    }
}