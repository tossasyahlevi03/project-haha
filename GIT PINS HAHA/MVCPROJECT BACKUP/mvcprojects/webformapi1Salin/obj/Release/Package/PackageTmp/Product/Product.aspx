﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="webformapi1.Product.Product" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="styleproduct.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />

	<script src="../ASSET/bootstrap.js" type="text/javascript"></script>
    <script src="../ASSET/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.8.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div id="wrapper">
    <div class="breadcrumb">
      <a href="Product.aspx">Management Products </a>  / <a href="#" data-toggle="modal" data-target="#myModal">Tambah Products</a>
    </div>




          
         <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Product</h4>
        </div>
        <div class="modal-body">
           <div class="box">
             <div class="box-isi">
               Product ID
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtproductid"></asp:TextBox>
             </div>
             <div class="box-isi">
               Nama Product
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtproductname"></asp:TextBox>
             </div>
             <div class="box-isi">
               Period
                 <asp:DropDownList runat="server" CssClass="dropdown" ID="dd" EnableViewState="true" AutoPostBack="true">
                      <asp:ListItem>--Select Period--</asp:ListItem>
                     <asp:ListItem>Dayly</asp:ListItem>
                    <asp:ListItem>Weekly</asp:ListItem>
                      <asp:ListItem>Monthly</asp:ListItem>
                    <asp:ListItem>Anually</asp:ListItem>
                 </asp:DropDownList>
             </div>
             <div class="box-isi">
               Harga
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtharga"></asp:TextBox>
             </div>
           
            
               
               
                  <div class="box-isi">
              
                 <asp:Button runat="server" ID="btnisi" CssClass="btn22" Text="Submit" OnClick="btnisi_Click1"   />
                      <label id="Label1" runat="server"></label>
             </div>
         </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


            <div class="box-data">

            <div class="box-control">
                  <div class="container">
                      <asp:TextBox runat="server" ID="txtcari" placeholder="Search By Invoice Number" CssClass="ss"></asp:TextBox>
                        <div class="input-group-append">

       <asp:Button runat="server" ID="Button1" Text="Cari" CssClass="btn"  data-toggle="modal" data-target="#exampleModal" />

</div>
                 

            </div>
               
               </div>
       
        <div class="dg1">
                        <label runat="server" id="lberror"></label>

            <asp:GridView runat="server" ID="dgrapor" PageSize="4" OnPageIndexChanging="dgrapor_PageIndexChanging" CssClass="mygridview" AllowPaging="True" AutoGenerateColumns="false" CellPadding="4" ForeColor="#333333" GridLines="None">
                <Columns>
                 <asp:BoundField DataField="product_id" HeaderText="Product ID" />
                   <asp:BoundField DataField="name" HeaderText="Product Name" />
                      <asp:BoundField DataField="period" HeaderText="Period" />
                   <asp:BoundField DataField="fee_per_period" HeaderText="Harga Asli" />
   <asp:BoundField DataField="disc" HeaderText="Discount" />
                   <asp:BoundField DataField="afterDisc"   HeaderText="Harga Potong Diskon" />



       <asp:TemplateField>
              <HeaderTemplate>
            
              </HeaderTemplate>
              <ItemTemplate>
                   <asp:LinkButton runat="server" ID="links" Text="View" ></asp:LinkButton>
              </ItemTemplate>
        </asp:TemplateField>
  </Columns>
                
                <AlternatingRowStyle BackColor="White" />
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
                        </asp:GridView>
          </div>
      </div>
      </div>
</asp:Content>
