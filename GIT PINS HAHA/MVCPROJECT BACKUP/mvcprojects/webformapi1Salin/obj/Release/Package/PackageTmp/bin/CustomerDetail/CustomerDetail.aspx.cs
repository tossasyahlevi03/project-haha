﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using webformapi1.Models;
using System.Web.Script.Serialization;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Data;
namespace webformapi1.CustomerDetail
{
    public partial class CustomerDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Get_Customer();
                string nik = Session["nik"].ToString();
                txtadminowner.Text = nik;
                mappemai();
                list();
               
            }
        }


      public void list()
        {
            List<string> province = new List<string>()
                {"--Pilih Provinsi--",
                   "ACEH",
   "SUMATERA UTARA",
   "SUMATERA BARAT ",
  "RIAU",
  "JAMBI",
  "SUMATERA SELATAN",
  "BENGKULU",
   "LAMPUNG",
  "`KEPULAUAN BANGKA BELITUNG",
   "KEPULAUAN RIAU",
  "DKI JAKARTA",
  "JAWA BARAT",
   "JAWA TENGAH",
  "DI YOGYAKARTA",
   "JAWA TIMUR",
   "BANTEN",
   "BALI",
   "NUSA TENGGARA BARAT",
   "NUSA TENGGARA TIMUR",
   "KALIMANTAN BARAT",
   "KALIMANTAN TENGAH",
  "KALIMANTAN SELATAN",
   "KALIMANTAN TIMUR",
   "KALIMANTAN UTARA",
  "SULAWESI UTARA",
  "SULAWESI TENGAH",
  "SULAWESI SELATAN",
"SULAWESI TENGGARA",
   "GORONTALO",
   "SULAWESI BARAT",
 "MALUKU",
   "MALUKU UTARA",
 "PAPUA BARAT",
   "PAPUA"
           
        };

            foreach (string pr in province)
            {
                dppr.Items.Add(pr);
            }

        }

        public void Get_Customer()
        {
           try
            {
                HttpClient clients = new HttpClient();

                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("customer/getCustomers").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    var ss = (new JavaScriptSerializer()).Deserialize<List<Customer>>(s);
                    if (ss==null)
                    {
                        DataTable dt = new DataTable();

                        dgrapor.DataSource = dt;
                        dgrapor.DataBind();
                    }
                    else
                    {
                        dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Customer>>(s);
                        dgrapor.DataBind();
                    }                   
                }
                else
                {
                    lberror.InnerText = rs.ReasonPhrase;

                }
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable();

                dgrapor.DataSource = dt;
                dgrapor.DataBind();
                lberror.InnerText = ex.Message;

            }


        }

        protected void dgrapor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgrapor.PageIndex = e.NewPageIndex;
            this.Get_Customer();
        }

        public void getnik()
        {
            try
            {
                Customer cd = new Customer { nik = txtcari.Text };
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.PostAsJsonAsync("customer/getCustomer",cd).Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    var ss = (new JavaScriptSerializer()).Deserialize<List<Customer>>(s);
                    if (ss == null)
                    {
                        DataTable dt = new DataTable();
                        dgrapor.DataSource = dt;
                        dgrapor.DataBind();
                    }
                    else
                    {
                        dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Customer>>(s);
                        dgrapor.DataBind();
                    }
                }
                else
                {
                    lberror.InnerText = rs.ReasonPhrase;

                }
            }
            catch (Exception ex)
            {
               
                lberror.InnerText = ex.Message;

            }


        }

        public void mappemai()
        {
            try
            {
              
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("admin/getEmailRoleAdmin").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    var ss = JsonConvert.DeserializeObject<List<string>>(s);
                    dpp.DataSource = ss;
                    dpp.DataBind();
                }
                else
                {
                    lberror.InnerText = rs.ReasonPhrase;

                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }

        public void carinik()
        {
            try
            {
                // Cdr cd = new Cdr { nik = txtcari.Text };
                string url = "http://10.0.30.76:8080/";
                string param = "customer/getCustomer?nik=" + txtcari.Text + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync(param).Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    dgrapor.DataSource = (new JavaScriptSerializer()).Deserialize<List<Customer>>(s);
                    dgrapor.DataBind();

                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;


                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }

        public void simpancustomer()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Customer cd = new Customer { nik = txtnik.Text, name= txtnamacustomer.Text, address= txtaddress.Text, city= txtcity.Text, province= dppr.SelectedValue, pic_name= txtpicname.Text, pic_phone_number= txtpicphone.Text, pic_email= dpp.SelectedValue, admin_owner=txtadminowner.Text };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/customer/");
                    var response = client.PostAsJsonAsync("insertCustomer", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        txtnamacustomer.Text = txtcity.Text = txtaddress.Text  = txtpicname.Text = txtpicphone.Text = "";                      
                        Response.Write("<script>alert(' Penyimpanan Berhasil' );</script>");
                    }
                    else
                    {
                        lberror.InnerText = response.ReasonPhrase +"Periksa Koneksi Anda Tidak Dapat Terhubung Ke API ";
                    }
                }
            }
            catch (Exception ex)
            {
                lberror.InnerText = ex.Message;
            }
        }


        public void update()
        {
            using (var client = new HttpClient())
            {
                Customer cd = new Customer{ nik = TextBox1.Text, name= TextBox2.Text, address= TextBox3.Text, city= TextBox4.Text, province= TextBox5.Text, pic_name= TextBox6.Text, pic_phone_number= TextBox7.Text, pic_email= TextBox8.Text, admin_owner=TextBox9.Text };
                client.BaseAddress = new Uri("http://10.0.30.76:8080/customer/");
                var response = client.PostAsJsonAsync("updateCustomer", cd).Result;
                if (response.IsSuccessStatusCode)
                {
                    // lb.InnerText = "koneksi";
                    var s = response.Content.ReadAsStringAsync().Result;
                    Response.Write(s);
                    Get_Customer();
                }
                else
                {
                    Response.Write("<script>alert('" + response.ReasonPhrase + "');</script>");
                }
            }
        }
        public void delete()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Customer cd = new Customer { nik = TextBox1.Text };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/customer/");
                    var response = client.PostAsJsonAsync("deleteCustomer", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        txtnamacustomer.Text = txtcity.Text = txtaddress.Text  = txtpicname.Text = txtpicphone.Text =  "";
                        Response.Write("<script>alert(' Penyimpanan Berhasil' );</script>");
                    }
                    else
                    {
                        lberror.InnerText = response.ReasonPhrase + "Periksa Koneksi Anda Tidak Dapat Terhubung Ke API ";
                    }
                }
            }
            catch (Exception ex)
            {
                lberror.InnerText = ex.Message;
            }
        }

        protected void btnisi_Click(object sender, EventArgs e)
        {
            simpancustomer();
            Get_Customer();
        }

        protected void dpp_SelectedIndexChanged(object sender, EventArgs e)
        {
            //lbs.Text = dpp.SelectedValue;


        }

        protected void pilih_Click(object sender, EventArgs e)
        {
            //string a = dpp.SelectedValue;
            //lbs.Text = a;
        }


        public void getnamephone()
        {
            try
            {
                // Cdr cd = new Cdr { nik = txtcari.Text };
                string url = "http://10.0.30.76:8080/";
                string param = "admin/getNamePhoneByEmail?email=" + dpp.SelectedValue + "";
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync(param).Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    var person = JsonConvert.DeserializeObject<Cdr>(s);
                    txtpicphone.Text = person.phone_number;
                    txtpicname.Text = person.first_name + " " + person.last_name;

                }
                else
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lberror.InnerText = s;


                }
            }
            catch (Exception ex)
            {

                lberror.InnerText = ex.Message;

            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            delete();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            update();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            carinik();
        }

        protected void btns_Click(object sender, EventArgs e)
        {
            getnamephone();
        }
    }
}