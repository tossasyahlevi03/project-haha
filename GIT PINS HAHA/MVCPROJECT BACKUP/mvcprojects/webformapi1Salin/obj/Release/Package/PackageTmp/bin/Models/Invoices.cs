﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webformapi1.Models
{
    public class Invoices
    {
        public string invoice_number { get; set; }
        public string customer_nik{ get; set; }
        public int product_id { get; set; }
        public string product_name { get; set; }
        public string period { get; set; }
        public DateTime invoice_date { get; set; }
        public string payment_gateway { get; set; }
        public float amount { get; set; }
        public string status { get; set; }
    }
}