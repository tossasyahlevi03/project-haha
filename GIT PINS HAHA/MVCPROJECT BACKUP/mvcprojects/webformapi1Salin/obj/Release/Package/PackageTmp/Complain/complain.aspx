﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="complain.aspx.cs" Inherits="webformapi1.Complain.complain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="stylecomplain.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />

    <script src="../ASSET/bootstrap.js" type="text/javascript"></script>
    <script src="../ASSET/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.8.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="wrapper">
        <div class="breadcrumb">
            <a href="complain.aspx">Customer Complain</a> / <a href="#" data-toggle="modal" data-target="#myModal">Tambah Complain</a>
        </div>

        <div id="myModal2" class="modal fade">
	        <div class="modal-dialog">

		        <!-- Modal content -->
		        <div class="modal-content">
			        <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Manipulasi Data User</h4>
			        </div>
			
			        <div class="modal-body">
				        <div class="box">
					        <div class="box-isi">
						        ID
						        <asp:TextBox runat="server" CssClass="form-control" ID="TextBox1"></asp:TextBox>
					        </div>
					
                            <div class="box-isi">
                                Type
                                <asp:DropDownList runat="server" ID="DropDownList1">
                                    <asp:ListItem>--Select Role--</asp:ListItem>
                                     <asp:ListItem>Complain</asp:ListItem>
                                     <asp:ListItem>Issue</asp:ListItem>
                                     <asp:ListItem>News</asp:ListItem>
                                </asp:DropDownList>
                            </div>
					
					        <div class="box-isi">
						        From
						        <asp:TextBox runat="server" CssClass="form-control" ID="TextBox2"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        To
						        <asp:TextBox runat="server" CssClass="form-control" ID="TextBox3"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Date
						        <asp:TextBox runat="server" CssClass="form-control" ID="TextBox4"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Subject
						        <asp:TextBox runat="server" CssClass="form-control" ID="TextBox5"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Content
                                <textarea runat="server" class="form-control" id="Textarea1"></textarea>
					        </div>
					
					        <div class="box-isi">
						        Contact
						        <asp:TextBox runat="server" CssClass="form-control" ID="TextBox6"></asp:TextBox>
					        </div>
						
					        <div class="box-isi">
						        Attachment
                                <asp:FileUpload runat="server" ID="upload" /><asp:Button runat="server" ID="btnloadpc" CssClass="btn btn-default" Text="Upload" OnClick="btnloadpc_Click" />
						       <label runat="server" id="lbpathfile"></label>
					        </div>
				        </div>
			        </div>
        
			        <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
		        </div>
            </div>
        </div>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
		        <!-- Modal content-->
		        <div class="modal-content">
			        <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Tambah Complain</h4>
			        </div>
			
			        <div class="modal-body">
				        <div class="box">
					        <div class="box-isi">
						        ID
						        <asp:TextBox runat="server" CssClass="form-control" ID="id"></asp:TextBox>
					        </div>
					
                            <div class="box-isi">
                                Type
                                <asp:DropDownList runat="server" ID="dprole">
                                    <asp:ListItem>--Select Role--</asp:ListItem>
                                     <asp:ListItem>Complain</asp:ListItem>
                                     <asp:ListItem>Issue</asp:ListItem>
                                     <asp:ListItem>News</asp:ListItem>
                                </asp:DropDownList>
                            </div>
					
					        <div class="box-isi">
						        From
						        <asp:TextBox runat="server" CssClass="form-control" ID="from"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        To
						        <asp:TextBox runat="server" CssClass="form-control" ID="to"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Date
						        <asp:TextBox runat="server" CssClass="form-control" ID="date"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Subject
						        <asp:TextBox runat="server" CssClass="form-control" ID="subject"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Content
                                <textarea runat="server" class="form-control" id="content"></textarea>
					        </div>
					
					        <div class="box-isi">
						        Contact
						        <asp:TextBox runat="server" CssClass="form-control" ID="contact"></asp:TextBox>
					        </div>
						
					        <div class="box-isi">
						        Attachment
                                <asp:FileUpload runat="server" ID="FileUpload1" /><asp:Button runat="server" ID="Button2" CssClass="btn btn-default" Text="Upload" OnClick="btnloadpc_Click" />
						       <label runat="server" id="Label2"></label>
					        </div>
                            
                            <div class="box-isi">
						        <asp:Button runat="server" ID="btnisi" CssClass="btn22" Text="Submit"  />
						        <label id="Label1" runat="server"></label>
					        </div>
				        </div>
			        </div>
				
			        <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
		        </div>
	        </div>
        </div>

        <div class="box-data">
	        <div class="box-control">
                <asp:DropDownList runat="server" EnableViewState="false" ID="dp_active" CssClass="drops" AutoPostBack="true" >
			        <asp:ListItem>--Select Status--</asp:ListItem>
			        <asp:ListItem>COMPLAINT</asp:ListItem>
			        <asp:ListItem>NEWS</asp:ListItem>
                </asp:DropDownList>

                <div class="container">
                    <asp:TextBox runat="server" ID="txtcari" placeholder="Search By Nik" CssClass="ss"></asp:TextBox>
            
			        <div class="input-group-append">
				        <asp:Button runat="server" ID="Button1" Text="Cari" CssClass="btn" data-toggle="modal" data-target="#exampleModal" />
			        </div>
                </div>
	        </div>
	
	        <div class="table-responsive">
		        <label runat="server" id="lberror"></label>
        
		        <asp:GridView runat="server" AllowPaging="True" PageSize="4" EmptyDataText="Tidak ADA dATA" ID="dgrapor"   CssClass="mygridview"  AlternatingRowStyle-CssClass="alt"  PagerStyle-CssClass="pgr"  CellPadding="4"         AutoGenerateColumns="False" ForeColor="#333333" GridLines="None">
			        <AlternatingRowStyle CssClass="alt" BackColor="White"></AlternatingRowStyle>
            
			        <Columns>
                        <asp:BoundField DataField="report_id" HeaderText="ID" />
                        <asp:BoundField DataField="report_type" HeaderText="Type" />
                        <asp:BoundField DataField="froms" HeaderText="From" />
                        <asp:BoundField DataField="tos" HeaderText="To" />
                        <asp:BoundField DataField="dates" HeaderText="Date" />
				        <asp:BoundField DataField="subject" HeaderText="Subject" />
                       
			           <asp:TemplateField>
					          <HeaderTemplate>
						        <%--                  <a href="#" data-toggle="modal" data-target="#myModal">Tambah User</a>--%>
					          </HeaderTemplate>
					          <ItemTemplate>
						           <asp:LinkButton runat="server" ID="links" Text="View" ></asp:LinkButton>
					          </ItemTemplate>
				        </asp:TemplateField>
			        </Columns>
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
			        <EmptyDataTemplate>No Record Available</EmptyDataTemplate>  
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <SortedAscendingCellStyle BackColor="#FDF5AC" />
                    <SortedAscendingHeaderStyle BackColor="#4D0000" />
                    <SortedDescendingCellStyle BackColor="#FCF6C0" />
                    <SortedDescendingHeaderStyle BackColor="#820000" />

                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
