﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using webformapi1.Models;
using System.IO;

namespace webformapi1.Dashboard
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                count_news();
                count_complaint();
                Get_Count_User();
                Get_Count_Customer();
                Get_Count_Invoice();
                Get_Count_Product();
                Get_Count_lICENCE();
                txtdate.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
                txtfrom.Text = Session["email"].ToString();
                txtcontact.Text = Session["phone"].ToString();
                txtid.Text = Session["email"].ToString() + DateTime.Now.ToString("hh:mm:ss");
            }
        }

   

        public string filepath { get; set; }

        public void Get_Count_Product()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("product/countProducts").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    Label9.Text = s.ToString();
                }
                else
                {
                    Label9.Text= rs.ReasonPhrase;
                }
            }
            catch (Exception ex)
            {
                Label9.Text = ex.Message;

            }
        }

        public void Get_Count_Invoice()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("invoices/countInvoices").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                   Label2.Text = s.ToString();
                }
                else
                {
                  Label2.Text=rs.ReasonPhrase;
                }
            }
            catch (Exception ex)
            {
                Label2.Text = ex.Message;
            }
        }

        public void Get_Count_Customer()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("customer/countCustomers").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                   Label1.Text = s.ToString();
                }
                else
                {
                    Label1.Text = rs.ReasonPhrase;
                }
            }
            catch (Exception ex)
            {
                Label1.Text = ex.Message;
            }
        }


        public void Get_Count_lICENCE()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("license/countLicenses").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    Label3.Text = s.ToString();
                }
                else
                {
                    Label3.Text = rs.ReasonPhrase;

                }
            }
            catch (Exception ex)
            {
                Label3.Text = ex.Message;

            }
        }

        public void Get_Count_User()
        {
            try
            {
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.GetAsync("admin/countAdmins").Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    lbcount1.Text = s.ToString();
                }
                else
                {
                    lbcount1.Text = rs.ReasonPhrase;

                }
            }
            catch (Exception ex)
            {
                lbcount1.Text = ex.Message;

            }
        }

        public void count_complaint()
        {
            try
            {
                complain c = new complain { tos = Session["email"].ToString() };
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.PostAsJsonAsync("report/countComplaintByReceiver",c).Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    Label4.Text = s.ToString();
                }
                else
                {
                    Label4.Text = rs.ReasonPhrase;

                }
            }
            catch (Exception ex)
            {
                Label4.Text = ex.Message;

            }
        }

        public void count_news()
        {
            try
            {
                complain c = new complain { tos = Session["email"].ToString() };
                HttpClient clients = new HttpClient();
                clients.BaseAddress = new Uri("http://10.0.30.76:8080/");
                clients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage rs = clients.PostAsJsonAsync("report/countNewsByReceiver", c).Result;
                if (rs.IsSuccessStatusCode)
                {
                    var s = rs.Content.ReadAsStringAsync().Result;
                    Label5.Text = s.ToString();
                }
                else
                {
                    Label5.Text = rs.ReasonPhrase;

                }
            }
            catch (Exception ex)
            {
                Label5.Text = ex.Message;

            }
        }

        public void simpan()
        {
          
            try
            {
                using (var client = new HttpClient())
                {
                    webformapi1.Models.complain cd = new webformapi1.Models.complain { report_id= txtid.Text, report_type=dpp.SelectedValue, froms=txtfrom.Text, tos=txtto.Text, dates=DateTime.Parse(this.txtdate.Text), subject=txtsubject.Text, contents=txtconten.InnerText, contacts=txtcontact.Text, attachment= lbpath.Text };
                    client.BaseAddress = new Uri("http://10.0.30.76:8080/report/");
                    var response = client.PostAsJsonAsync("insertReport", cd).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // lb.InnerText = "koneksi";

                        txtid.Text = "";
                       
                        txtto.Text = "";
                       
                        txtsubject.Text = "";
                        txtconten.InnerText = "";
                        lbpath.Text = "";
                        Response.Write("<script>alert(' Penyimpanan Berhasil' );</script>");


                    }
                    else
                    {
                        lb.Text = response.ReasonPhrase;
                    }
                }
            }
            catch (Exception ex)
            {
               lb.Text = ex.Message;

            }
        }

        protected void btns_Click(object sender, EventArgs e)
        {
            string folderpath = Server.MapPath("~/IMAGES2/");
            if (!Directory.Exists(folderpath))
            {
                Directory.CreateDirectory(folderpath);

            }
            var fp = Path.Combine(folderpath, Path.GetFileName(upload.FileName));

            upload.SaveAs(fp);


            filepath = folderpath + Path.GetFileName(upload.FileName);
            lbpath.Text = filepath;
        }

        protected void btnss_Click(object sender, EventArgs e)
        {
            simpan();
        }

       
    }
}