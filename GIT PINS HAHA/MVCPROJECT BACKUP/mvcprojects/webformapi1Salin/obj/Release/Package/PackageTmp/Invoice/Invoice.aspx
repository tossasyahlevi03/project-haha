﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="Invoice.aspx.cs" Inherits="webformapi1.Invoice.Invoice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="styleinvoice.css" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="wrapper">
		<div class="breadcrumb">
			<a href="Invoice.aspx">Management Invoices </a>  / <a href="#" data-toggle="modal" data-target="#myModal">Tambah Invoices</a>
		</div>
        <div class="modal fade" id="myModal2" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Tambah Invoice</h4>
					</div>
					<div class="modal-body">
						<div class="box">
							<div class="box-isi">
								Customer Nik
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox1"></asp:TextBox>
							</div>
							<div class="box-isi">
								Product Id
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox2"></asp:TextBox>
							</div>
							<div class="box-isi">
								Product Name
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox3"></asp:TextBox>
							</div>
							<div class="box-isi">
								Period
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox4"></asp:TextBox>
							</div>
							<div class="box-isi">
								Payment Gateway
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox5"></asp:TextBox>
							</div>
							<div class="box-isi">
								Amount
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox6"  TextMode="Date"></asp:TextBox>
							</div>
							<div class="box-isi">
								Status
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox9" ></asp:TextBox>
							</div>
							<div class="box-isi">
								<asp:Button runat="server" ID="Button2" CssClass="btn22" Text="Submit"   />
								<label id="Label2" runat="server"></label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>     
			</div>
		</div>
        <div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Tambah Invoice</h4>
					</div>
					<div class="modal-body">
					   <div class="box">
							<div class="box-isi">
								Nomor Invoice
								<asp:TextBox runat="server" CssClass="form-control" ID="txtnoinvoice"></asp:TextBox>
							</div>
							<div class="box-isi">
								Nama Customer
								<asp:TextBox runat="server" CssClass="form-control" ID="txtnamacustomer"></asp:TextBox>
							</div>
							<div class="box-isi">
								ID Product
								<asp:TextBox runat="server" CssClass="form-control" ID="txtidproduct"></asp:TextBox>
							</div>
							<div class="box-isi">
								Product Name
								<asp:TextBox runat="server" CssClass="form-control" ID="txtproductname"></asp:TextBox>
							</div>
							<div class="box-isi">
								Period
								<asp:TextBox runat="server" CssClass="form-control" ID="txtperiod"></asp:TextBox>
							</div>
							<div class="box-isi">
								Tanggal Invoice
								<asp:TextBox runat="server" CssClass="form-control" ID="txtdateinvoice"  TextMode="Date"></asp:TextBox>
							</div>
							<div class="box-isi">
								Payment Gateway
								<asp:TextBox runat="server" CssClass="form-control" ID="txtpayment" ></asp:TextBox>
							</div>
							<div class="box-isi">
								Amount
								<asp:TextBox runat="server" CssClass="form-control" ID="txtamount" ></asp:TextBox>
							</div>
							<div class="box-isi">
								Status
								<asp:TextBox runat="server" CssClass="form-control" ID="txtstatus" ></asp:TextBox>
							</div>
							<div class="box-isi">
								<asp:Button runat="server" ID="btnisi" CssClass="btn22" Text="Submit"   />
								<label id="Label1" runat="server"></label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>     
			</div>
		</div>
        <div class="box-data">
			<div class="box-control">
				<asp:DropDownList runat="server" EnableViewState="false" ID="dp_active" CssClass="drops" AutoPostBack="true">
                    <asp:ListItem>--Select Status--</asp:ListItem>
                     <asp:ListItem>Paid</asp:ListItem>
                    <asp:ListItem>UnPaid</asp:ListItem>
                </asp:DropDownList>
                <div class="container">
					<asp:TextBox runat="server" ID="txtcari" placeholder="Search By Invoice Number" CssClass="ss"></asp:TextBox>
                    <div class="input-group-append">
						<asp:Button runat="server" ID="Button1" Text="Cari" CssClass="btn"  data-toggle="modal" data-target="#exampleModal" />
					</div>
				</div>
            </div>       
			<div class="dg1">
				<label runat="server" id="lberror"></label>
				<asp:GridView runat="server" AllowPaging="True" PageSize="4" EmptyDataText="Tidak ADA dATA" ID="dgrapor" CssClass="mydatagrid" CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="dgrapor_PageIndexChanging" AutoGenerateColumns="False">
					<Columns>
						<asp:BoundField DataField="invoice_number" HeaderText="invoice_number" />
						<asp:BoundField DataField="customer_nik" HeaderText="Customer" />
						<asp:BoundField DataField="product_id" HeaderText="Product ID" />
						<asp:BoundField DataField="product_name" HeaderText="Product Name" />
						<asp:BoundField DataField="period" HeaderText="Period" />
						<asp:BoundField DataField="invoice_date" DataFormatString="{0:MM/dd/yyyy}"  HeaderText="Tanggal Invoice" />
						<asp:BoundField DataField="payment_gateway" HeaderText="Payment" />
						<asp:BoundField DataField="amount" HeaderText="Amount" />
						<asp:BoundField DataField="status" HeaderText="Status" />
						<asp:TemplateField>
							<HeaderTemplate>
								<%--                  <a href="#" data-toggle="modal" data-target="#myModal">Tambah User</a>--%>
							</HeaderTemplate>
							<ItemTemplate>
								<asp:LinkButton runat="server" ID="links" Text="View" ></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<AlternatingRowStyle BackColor="White" />
					<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
					<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
					<PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
					<RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
					<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
					<SortedAscendingCellStyle BackColor="#FDF5AC" />
					<SortedAscendingHeaderStyle BackColor="#4D0000" />
					<SortedDescendingCellStyle BackColor="#FCF6C0" />
					<SortedDescendingHeaderStyle BackColor="#820000" />
				</asp:GridView>
			</div>  
		</div>
    </div>
</asp:Content>
