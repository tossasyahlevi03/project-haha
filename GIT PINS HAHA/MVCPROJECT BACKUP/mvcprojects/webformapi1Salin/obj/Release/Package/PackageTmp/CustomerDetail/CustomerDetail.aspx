﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="CustomerDetail.aspx.cs" Inherits="webformapi1.CustomerDetail.CustomerDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="stylecustomerdetail.css" type="text/css" />
    <link rel="stylesheet" href="styleuser.css" type="text/css" />
	<link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        $(function () {
            $("[id*=dgrapor").find("[id*=links]").click(function () {
                //Reference the GridView Row.
                var row = $(this).closest("tr");
                $("#myModal2").modal();
               
                //Determine the Row Index.
                var message = "Row Index: " + (row[0].rowIndex - 1);

                //Read values from Cells.
             
                var nik = row.find("td").eq(0).html();
                var namacustomer= row.find("td").eq(1).html();
                var address = row.find("td").eq(2).html();
                var provinsi = row.find("td").eq(3).html();
                var city = row.find("td").eq(4).html();
                var picname = row.find("td").eq(5).html();
                var picphone = row.find("td").eq(6).html();
                var picemail = row.find("td").eq(7).html();
                var adminowner = row.find("td").eq(8).html();


                  $("#lb2").val(status);

                $("#<%=TextBox1.ClientID%>").val(nik);
				$("#<%=TextBox2.ClientID%>").val(namacustomer);
                $("#<%=TextBox3.ClientID%>").val(address);
                $("#<%=TextBox4.ClientID%>").val(provinsi);
                $("#<%=TextBox5.ClientID%>").val(city);
                $("#<%=TextBox6.ClientID%>").val(picname);
				$("#<%=TextBox7.ClientID%>").val(picphone);
                $("#<%=TextBox8.ClientID%>").val(picemail);
                $("#<%=TextBox9.ClientID%>").val(adminowner);            

                //Reference the TextBox and read value.
               // message += "\nCountry: " + row.find("td").eq(2).find("input").eq(0).val();

                //Display the data using JavaScript Alert Message Box.
               // alert(message);
                return false;
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="wrapper">
		<div class="breadcrumb">
			<a href="User.aspx">Customer Detail</a> / <a href="#" data-toggle="modal" data-target="#myModal">Tambah Customer</a>  
		</div>
        <div id="myModal2" class="modal fade">
            <div class="modal-dialog">
				<!-- Modal content -->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Form Manipulasi Data Customer</h4>
					</div>
					<div class="modal-body">
						<div class="box">
							<div class="box-isi">
								NIK
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox1"></asp:TextBox>
							</div>
							<div class="box-isi">
								Nama Customer
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox2"></asp:TextBox>
							</div>
							<div class="box-isi">
								Address
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox3"></asp:TextBox>
							</div>
							<div class="box-isi">
								Provinsi
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox4"></asp:TextBox>
							</div>
							<div class="box-isi">
								City
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox5"></asp:TextBox>
							</div>
							<div class="box-isi">
								PIC_Name
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox6"></asp:TextBox>
							</div>
							<div class="box-isi">
								PIC_Phone
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox7"></asp:TextBox>
							</div>
							<div class="box-isi">
								PIC_Email
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox8" ></asp:TextBox>
							</div>
							<div class="box-isi">
								Admin Owner
								<asp:TextBox runat="server" CssClass="form-control" ID="TextBox9" Enabled="false"></asp:TextBox>
							</div>
							<div class="box-isi">						  
								<asp:Button runat="server" ID="Button3" CssClass="btn btn-default" Text="Update" OnClick="Button3_Click"  />
								<asp:Button runat="server" ID="Button4" CssClass="btn btn-default" Text="Delete" OnClick="Button4_Click"   />
								<label id="Label2" runat="server"></label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Tambah Customer</h4>
					</div>
					<div class="modal-body">
						<div class="box">
							<div class="box-isi">
								NIK
								<asp:TextBox runat="server" CssClass="form-control" ID="txtnik"></asp:TextBox>
							</div>
							<div class="box-isi">
								Nama Customer
								<asp:TextBox runat="server" CssClass="form-control" ID="txtnamacustomer"></asp:TextBox>
							</div>
							<div class="box-isi">
								Address
								<asp:TextBox runat="server" CssClass="form-control" ID="txtaddress"></asp:TextBox>
							</div>
							<div class="box-isi">
								Provinsi
								<div class="dropdown show">
									<asp:DropDownList runat="server" ID="dppr" CssClass=" btn-secondary dropdown-toggle"></asp:DropDownList>
								</div>
							</div>
							<div class="box-isi">
								Kota / Kabupaten
								<div class="dropdown show">
									<asp:DropDownList runat="server" ID="Ddpkota" CssClass=" btn-secondary dropdown-toggle"></asp:DropDownList>
								</div>
							</div>
							<div class="box-isi">
								City
								<asp:TextBox runat="server" CssClass="form-control" ID="txtcity"></asp:TextBox>
							</div>
							<div class="box-isi">
								PIC Email
								<div class="dropdown show">
									<asp:DropDownList CssClass=" dropdown" runat="server" ID="dpp" OnSelectedIndexChanged="dpp_SelectedIndexChanged" EnableViewState="true" >
										<asp:ListItem>--Select Email--</asp:ListItem> 
									</asp:DropDownList>
									<asp:Button runat="server" ID="btns" OnClick="btns_Click" Text="OK" CssClass="btn btn-default" />
								</div>
								<%-- <asp:Button runat="server" ID="pilih" Text="pilih" OnClick="pilih_Click" />
								<asp:Label runat="server" ID="lbs"></asp:Label>--%>
							</div>
							<div class="box-isi">
								PIC_Name
								<asp:TextBox runat="server" CssClass="form-control" ID="txtpicname"></asp:TextBox>
							</div>
							<div class="box-isi">
								PIC_Phone
								<asp:TextBox runat="server" CssClass="form-control" ID="txtpicphone"></asp:TextBox>
							</div>
							<%-- <div class="box-isi">
								PIC_Email
								<asp:TextBox runat="server" CssClass="form-control" ID="txtpicemail" ></asp:TextBox>
							</div>--%>
							<div class="box-isi">
								Admin Owner
								<asp:TextBox runat="server" CssClass="form-control" ID="txtadminowner" Enabled="false"></asp:TextBox>
							</div>
							<div class="box-isi">
								<asp:Button runat="server" ID="btnisi" CssClass="btn btn-default" Text="Submit" OnClick="btnisi_Click"  />
								<label id="Label1" runat="server"></label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>    
			</div>
		</div>
        <div class="box-data">
			<div class="box-control">
				<asp:DropDownList runat="server" EnableViewState="false" ID="dp_active" CssClass="drops" AutoPostBack="true" >
					<asp:ListItem>--Select Status--</asp:ListItem>
                    <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>INACTIVE</asp:ListItem>
                </asp:DropDownList>                  
				<div class="container">
					<asp:TextBox runat="server" ID="txtcari" placeholder="Search By Nik" CssClass="ss"></asp:TextBox>
                    <div class="input-group-append">
						<asp:Button runat="server" ID="Button1" Text="Cari" CssClass="btn btn-default"  OnClick="Button1_Click" />
					</div>
                </div>
			</div>
			<div class="table-responsive">
				<label runat="server" id="lberror"></label>
				<asp:GridView runat="server" ID="dgrapor" EmptyDataText="No Data" PageSize="4"  AllowPaging="true"  OnPageIndexChanging="dgrapor_PageIndexChanging" CssClass="mydatagrid"  AlternatingRowStyle-CssClass="alt"  PagerStyle-CssClass="pgr" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">             
					<Columns>
						<asp:BoundField DataField="nik" HeaderText="NIK" />
						<asp:BoundField DataField="name" HeaderText="Customer" />
						<asp:BoundField DataField="address" HeaderText="Alamat" />
						<asp:BoundField DataField="city" HeaderText="Kota" />
						<asp:BoundField DataField="province" HeaderText="Provinsi" />
						<asp:BoundField DataField="pic_name" HeaderText="PIC Name" />
                        <asp:BoundField DataField="pic_phone_number" HeaderText="PIC Phone Number" />
						<asp:BoundField DataField="pic_email" HeaderText="PIC Email" />
                        <asp:BoundField DataField="admin_owner" HeaderText="Admin Owner" />
						<asp:TemplateField>
							<HeaderTemplate>
								<%--                  <a href="#" data-toggle="modal" data-target="#myModal">Tambah User</a>--%>
							</HeaderTemplate>
							<ItemTemplate>
								<asp:LinkButton runat="server" ID="links" Text="View" ></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<AlternatingRowStyle BackColor="White" />
					<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
					<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
					<PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
					<RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
					<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
					<SortedAscendingCellStyle BackColor="#FDF5AC" />
					<SortedAscendingHeaderStyle BackColor="#4D0000" />
					<SortedDescendingCellStyle BackColor="#FCF6C0" />
					<SortedDescendingHeaderStyle BackColor="#820000" />
                    <EmptyDataTemplate>No Record Available</EmptyDataTemplate>  
				</asp:GridView>
			</div>
        </div>
    </div>
</asp:Content>
