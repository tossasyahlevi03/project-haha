﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webformapi1.Models;
using Microsoft.AspNet.SignalR;

namespace webformapi1
{
    public class ChatHub:Hub
    {
        static List<Messages> mess = new List<Messages>();
        static List<Cdr> cd = new List<Cdr>();

        public void connect(string email)
        {
            var id = cd.FirstOrDefault();
            var ud = id.nik;

            if(cd.Count(x=>x.nik==ud)==0)
            {
                string logintime = DateTime.Now.ToString();

                Clients.Caller.onConnected(ud, logintime, mess);

                Clients.AllExcept(ud).onNewUserConnected(ud);
            }

        }

        public void sendmessagetoall(string nik, string message, string time)
        {
            AddMessageinCache(nik, message, time);

            Clients.All.messageReceived(nik, message, time);
        }

        private void AddMessageinCache(string nik,string message, string time)
        {
            mess.Add(new Messages { UserName = nik, Message = message, Time = time, UserImage="" });
            if(mess.Count>100)
            {
                mess.RemoveAt(0);
            }


        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = cd.FirstOrDefault(x => x.nik== Context.ConnectionId);
            if (item != null)
            {
                cd.Remove(item);

                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id);

            }
            return base.OnDisconnected(stopCalled);
        }


    }
}