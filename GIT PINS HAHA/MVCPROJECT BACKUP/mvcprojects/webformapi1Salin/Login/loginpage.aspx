﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginpage.aspx.cs" Inherits="webformapi1.Login.loginpage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
        <meta charset="utf-8" />
     <meta http-equiv="Cache-Control" content="no-cache"/>
          <meta http-equiv="Pragma" content="no-cache"/>
          <meta http-equiv="Expires" content="0"/>
       <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
          <meta name="HandheldFriendly" content="true" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="stylelogin.css" type="text/css" />
       <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />
    <script src="../scripts/jquery-1.8.2.min.js"></script>
       <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />

    <script src="../ASSET/bootstrap.js" type="text/javascript"></script>
    <script src="../ASSET/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.8.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <link href="../ASSET/font.css" rel="stylesheet">

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
        <div class="row">
            <div class="column leftcol">
                <img src="../ASSET/pm-ultra-icon.png" class="poto"/>
            </div>

                        <div class="column">
             <div class="box-login">
                <div class="form-group">
                    <p>Welcome to Billing Pins</p>
                </div>
                <div class="form-group">
                    <asp:TextBox runat="server" ID="txtusername" autocomplete="off" CssClass="form-control" placeholder="Insert Your NIK "></asp:TextBox>
                </div>
                  <div class="form-group">
                    <asp:TextBox runat="server" ID="txtpassword" autocomplete="off" CssClass="form-control" placeholder="Insert Your Password" TextMode="Password" ></asp:TextBox>
              </div>
                 <div class="form-group">
                                     <asp:Button runat="server" Text="Login" ID="Button1" CssClass="btn" OnClick="btn_login_Click" />
                <a href="#" data-toggle="modal" data-target="#myModal2" style="color:red;">Forgot Password</a>       
                <label id="lb" runat="server" class="lberror"></label>  
                     </div>           
            </div>
        </div>


                     <div id="myModal2" class="modal fade">
                 <div class="modal-dialog">

  <!-- Modal content -->
   <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pulihkan Password</h4>
        </div>
        <div class="modal-body">
           <div class="box">
             <div class="box-isi">
                Email Registered
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtemail" autocomplete="off" placeholder="Insert Your Registered Email" ></asp:TextBox>
             </div>
           
            
                  <div class="box-isi">
              
                 <asp:Button runat="server" ID="Button2" CssClass="btn btn-default" Text="Send" OnClick="Button2_Click" />
                   
                             


                      <label id="Label2" runat="server"></label>
             </div>
         </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

</div>
             </div>
             </div>



            </div>
    </div>
    </form>
</body>
</html>
