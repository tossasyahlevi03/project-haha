﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage/Site1.Master" AutoEventWireup="true" CodeBehind="License.aspx.cs" Inherits="webformapi1.License.License" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="stylelicense.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap-theme.css" type="text/css" />
    <link rel="stylesheet" href="../ASSET/bootstrap.min.css" type="text/css" />

	<script src="../ASSET/bootstrap.js" type="text/javascript"></script>
    <script src="../ASSET/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.8.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="wrapper">
		<div class="breadcrumb">
			<a href="License.aspx">Management License </a>  / <a href="#" data-toggle="modal" data-target="#myModal">Tambah License</a>
		</div>
        <div id="myModal2" class="modal fade">
	        <div class="modal-dialog">

		        <!-- Modal content -->
		        <div class="modal-content">
			        <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Manipulasi Data License</h4>
			        </div>
			
			        <div class="modal-body">
				        <div class="box">
					        <div class="box-isi">
						        Customer NIK
						        <asp:TextBox runat="server" CssClass="form-control" ID="nik"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Product ID
						        <asp:TextBox runat="server" CssClass="form-control" ID="productid"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Product Name
						        <asp:TextBox runat="server" CssClass="form-control" ID="productname"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        License Number
						        <asp:TextBox runat="server" CssClass="form-control" ID="license"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Start Date
						        <asp:TextBox runat="server" CssClass="form-control" ID="start" TextMode="Date"></asp:TextBox>
					        </div>

					        <div class="box-isi">
						        End Date
						        <asp:TextBox runat="server" CssClass="form-control" ID="end" TextMode="Date"></asp:TextBox>
					        </div>
						
					        <div class="box-isi">
						        Status
						        <asp:TextBox runat="server" CssClass="form-control" ID="status"></asp:TextBox>
					        </div>
				        </div>
			        </div>
        
			        <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
		        </div>
            </div>
        </div>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
		        <!-- Modal content-->
		        <div class="modal-content">
			        <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Tambah License</h4>
			        </div>
			
			        <div class="modal-body">
				        <div class="box">
					        <div class="box-isi">
						        Customer NIK
						        <asp:TextBox runat="server" CssClass="form-control" ID="nik1"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Product ID
						        <asp:TextBox runat="server" CssClass="form-control" ID="productid1"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Product Name
						        <asp:TextBox runat="server" CssClass="form-control" ID="productname1"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        License Number
						        <asp:TextBox runat="server" CssClass="form-control" ID="license1"></asp:TextBox>
					        </div>
					
					        <div class="box-isi">
						        Start Date
						        <asp:TextBox runat="server" CssClass="form-control" ID="start1" TextMode="Date"></asp:TextBox>
					        </div>

					        <div class="box-isi">
						        End Date
						        <asp:TextBox runat="server" CssClass="form-control" ID="end1" TextMode="Date"></asp:TextBox>
					        </div>
						
					        <div class="box-isi">
						        Status
						        <asp:TextBox runat="server" CssClass="form-control" ID="status1"></asp:TextBox>
					        </div>

                            <div class="box-isi">
						        <asp:Button runat="server" ID="btnisi" CssClass="btn22" Text="Submit"  />
						        <label id="Label1" runat="server"></label>
					        </div>
				        </div>
			        </div>
				
			        <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
		        </div>
	        </div>
        </div>

        <div class="box-data">
	        <div class="box-control">
                <asp:DropDownList runat="server" EnableViewState="false" ID="dp_active" CssClass="drops" AutoPostBack="true" >
			        <asp:ListItem>--Select Status--</asp:ListItem>
			        <asp:ListItem>Active</asp:ListItem>
			        <asp:ListItem>NONACTIVE</asp:ListItem>
                </asp:DropDownList>

                <div class="container">
                    <asp:TextBox runat="server" ID="txtcari" placeholder="Search By Nik" CssClass="ss"></asp:TextBox>
            
			        <div class="input-group-append">
				        <asp:Button runat="server" ID="Button1" Text="Cari" CssClass="btn" data-toggle="modal" data-target="#exampleModal" />
			        </div>
                </div>
	        </div>
	
	        <div class="table-responsive">
		        <label runat="server" id="lberror"></label>
        
		        <asp:GridView runat="server" AllowPaging="True" PageSize="4" EmptyDataText="Tidak ADA dATA" ID="dgrapor"   CssClass="mydatagrid"  AlternatingRowStyle-CssClass="alt"  PagerStyle-CssClass="pgr"  CellPadding="4"         AutoGenerateColumns="False" ForeColor="#333333" GridLines="None">
			        <%--<AlternatingRowStyle CssClass="alt" BackColor="White"></AlternatingRowStyle>--%>
            
					<Columns>
						<asp:BoundField DataField="customer_nik" HeaderText="Customer NIK" />
						<asp:BoundField DataField="product_id" HeaderText="Product ID" />
						<asp:BoundField DataField="product_name" HeaderText="Nama Product" />
						<asp:BoundField DataField="license_number" HeaderText="License Number" />
						<asp:BoundField DataField="start_date" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Start Date" />
						<asp:BoundField DataField="end_date" DataFormatString="{0:MM/dd/yyyy}" HeaderText="End Date" />
                        <asp:BoundField DataField="status" HeaderText="Status" />
						<asp:TemplateField>
							<HeaderTemplate>
								<%--            	<a href="#" data-toggle="modal" data-target="#myModal">Tambah User</a>--%>
							</HeaderTemplate>
							<ItemTemplate>
							   <asp:LinkButton runat="server" ID="links" Text="View" ></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
			        <EmptyDataTemplate>No Record Available</EmptyDataTemplate>  
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <SortedAscendingCellStyle BackColor="#FDF5AC" />
                    <SortedAscendingHeaderStyle BackColor="#4D0000" />
                    <SortedDescendingCellStyle BackColor="#FCF6C0" />
                    <SortedDescendingHeaderStyle BackColor="#820000" />

                </asp:GridView>
            </div>
        </div>            
    </div>
</asp:Content>
